#!/bin/bash

# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText:  2023 PeppemrintOS Team  (peppermintosteam@proton.me)

PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"

# Set the working folder variable
uchinanchu="$(pwd)"

# This cleanup might be better served in the BldHelper*.sh script.
# Create the build folder, move into it removing stale mountpoints and files there.
[ -e fusato ] && [ ! -d fusato ] && rm -f fusato || [ ! -e fusato ] && mkdir fusato
cd fusato
umount $(mount | grep "${PWD}/chroot" | tac | cut -f3 -d" ") 2>/dev/null
for i in ./* ./.build ; do [ $i = ./cache ] && continue || rm -rf $i ; done


#Define as configurações do Live Build

lb config  noauto \
	--binary-images iso-hybrid \
	--architectures amd64 \
	--distribution daedalus \
	--initsystem sysvinit \
	--archive-areas "main contrib non-free non-free-firmware" \
	--mirror-bootstrap http://deb.devuan.org/merged \
	--parent-mirror-bootstrap http://deb.devuan.org/merged \
	--parent-mirror-chroot http://deb.devuan.org/merged \
	--parent-mirror-chroot-security http://deb.devuan.org/merged \
	--parent-mirror-binary http://deb.devuan.org/merged \
	--parent-mirror-binary-security http://deb.devuan.org/merged \
	--mirror-chroot http://deb.devuan.org/merged \
	--mirror-chroot-security http://deb.devuan.org/merged \
	--security false \
	--updates false \
	--backports false \
	--firmware-chroot true \
	--debian-installer live \
	--debian-installer-distribution daedalus \
	--debian-installer-gui true \
	--iso-preparer "PeppermintOS-https://peppermintos.com/" \
	--iso-publisher "Peppermint OS Team" \
	--iso-volume "Peppermint" \
	--image-name "PepOSserver" \
	--debian-installer-preseedfile preseed.cfg \
	--checksums sha512 \
	--zsync false \
	--win32-loader false \

     "${@}"


# Setup the chroot structure
mkdir -p $uchinanchu/fusato/config/includes.installer
mkdir -p $uchinanchu/fusato/config/config/package-lists
mkdir -p $uchinanchu/fusato/config/includes.chroot
mkdir -p $uchinanchu/fusato/config/hooks/normal
mkdir -p $uchinanchu/fusato/config/archives
mkdir -p $uchinanchu/fusato/config/includes.binary
mkdir -p $uchinanchu/fusato/config/includes.chroot/boot/grub
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/default
mkdir -p $uchinanchu/fusato/config/includes.installer/usr/share
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/firewalld/zones

# Install software
echo "#Install system packages.
zonefstoolspep
dmzonedtoolspep
libzbdpep1
sudo
task-ssh-server
task-web-server
sshguard
htop
fail2ban
whois
rkhunter
debsecan
net-tools
cockpit
cockpit-podman
podman
samba
cups
gvfs-backends
git
wget


" >> $uchinanchu/fusato/config/package-lists/package.list.chroot

# Copy single files and folder to the chroot
cp $uchinanchu/pepgrub/grub $uchinanchu/fusato/config/includes.chroot/etc/default
cp $uchinanchu/pephooks/normal/* $uchinanchu/fusato/config/hooks/normal
cp $uchinanchu/peprepo/* $uchinanchu/fusato/config/archives
cp $uchinanchu/pepfirewall/* $uchinanchu/fusato/config/includes.chroot/etc/firewalld/zones
cp $uchinanchu/pepinstaller/preseed/preseed.cfg $uchinanchu/fusato/config/includes.installer


# Copy recursive files and sub-directories, containing symlinks.
cp -r $uchinanchu/peploadersplash/boot $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peploadersplash/isolinux $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/pepgrub/themes $uchinanchu/fusato/config/includes.chroot/boot/grub
cp -r $uchinanchu/pepinstaller/graphics $uchinanchu/fusato/config/includes.installer/usr/share
cp -r $uchinanchu/pepinstaller/themes $uchinanchu/fusato/config/includes.installer/usr/share

lb build
