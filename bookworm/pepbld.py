#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText:  2023 PeppemrintOS Team  (peppermintosteam@proton.me)

import os
import subprocess
import sys
import shutil


os.environ["PATH"] = "/sbin:/usr/sbin:/usr/local/sbin:" + os.environ["PATH"]

# Set the working folder variable
uchinanchu = os.getcwd()

# This cleanup might be better served in the BldHelper*.sh script.
# Create the build folder, move into it removing stale mountpoints and files there.
if os.path.exists("fusato") and not os.path.isdir("fusato"):
    os.remove("fusato")
elif not os.path.exists("fusato"):
    os.mkdir("fusato")
os.chdir("fusato")
subprocess.run(["umount", "$(mount | grep '{}' | tac | cut -f3 -d' ')".format(os.path.join(os.getcwd(), "chroot"))], stderr=subprocess.DEVNULL)
for item in os.listdir():
    if item == "cache":
        continue
    shutil.rmtree(item, ignore_errors=True)

# Define as configurações do Live Build
lb_command = [
    "lb", "config", "noauto",
    "--binary-images", "iso-hybrid",
    "--architectures", "amd64",
    "--linux-flavours", "amd64",
    "--distribution", "bookworm",
    "--archive-areas", "main contrib non-free non-free-firmware",
    "--apt-recommends", "true",
    "--security", "true",
    "--updates", "true",
    "--backports", "true",
    "--debian-installer", "live",
    "--debian-installer-distribution", "bookworm",
    "--debian-installer-gui", "true",
    "--iso-preparer", "PeppermintOS-https://peppermintos.com/",
    "--iso-publisher", "Peppermint OS Team",
    "--iso-volume", "PepOS-server",
    "--image-name", "PepOS-Server",
    "--debian-installer-preseedfile", "preseed.cfg",
    "--checksums", "sha512",
    "--zsync", "false",
    "--win32-loader", "false"
]

subprocess.run(lb_command + sys.argv[1:], check=True)


# Setup the chroot structure
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "config", "package-lists"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.chroot"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "hooks", "normal"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "archives"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.binary"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "boot", "grub"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "etc", "default"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "share"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "etc"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "etc", "firewalld", "zones"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "grub"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "lib", "finish-install.d"), exist_ok=True)
os.makedirs(os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "usr", "local", "bin"), exist_ok=True)

# Install software
with open(os.path.join(uchinanchu, "fusato", "config", "package-lists", "package.list.chroot"), "a") as file:
    file.write("# Install system packages.\n")
    file.write("zonefstoolspep\n")
    file.write("dmzonedtoolspep\n")
    file.write("libzbdpep1\n")
    file.write("sudo\n")
    file.write("task-ssh-server\n")
    file.write("sshguard\n")
    file.write("htop\n")
    file.write("whois\n")
    file.write("rkhunter\n")
    file.write("debsecan\n")
    file.write("podman\n")
    file.write("nfs-common\n")
    file.write("firewalld\n")
    file.write("git\n")
    file.write("wget\n")
    file.write("curl\n")
    file.write("tuned\n")
    file.write("dialog\n")

# Copy single files and folder to the chroot
shutil.copy2(os.path.join(uchinanchu, "pepgrub", "grub"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "preseed", "grub"))
for item in os.listdir(os.path.join(uchinanchu, "pephooks", "normal")):
    shutil.copy2(os.path.join(uchinanchu, "pephooks", "normal", item), os.path.join(uchinanchu, "fusato", "config", "hooks", "normal"))
for item in os.listdir(os.path.join(uchinanchu, "peprepo")):
    shutil.copy2(os.path.join(uchinanchu, "peprepo", item), os.path.join(uchinanchu, "fusato", "config", "archives"))
for item in os.listdir(os.path.join(uchinanchu, "pepfirewall")):
    shutil.copy2(os.path.join(uchinanchu, "pepfirewall", item), os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "etc", "firewalld", "zones"))
shutil.copy2(os.path.join(uchinanchu, "pepinstaller", "preseed", "preseed.cfg"), os.path.join(uchinanchu, "fusato", "config", "includes.installer"))
for item in os.listdir(os.path.join(uchinanchu, "pepscripts")):
    shutil.copy2(os.path.join(uchinanchu, "pepscripts", item), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "lib", "finish-install.d"))
for item in os.listdir(os.path.join(uchinanchu, "pepinstaller", "scripts" , "debsrv")):
    shutil.copy2(os.path.join(uchinanchu, "pepinstaller", "scripts" , "debsrv", item), os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "usr", "local", "bin"))

# Copy recursive files and sub-directories, containing symlinks.
shutil.copytree(os.path.join(uchinanchu, "peploadersplash", "boot"), os.path.join(uchinanchu, "fusato", "config", "includes.binary", "boot"))
shutil.copytree(os.path.join(uchinanchu, "peploadersplash", "isolinux"), os.path.join(uchinanchu, "fusato", "config", "includes.binary", "isolinux"))
shutil.copytree(os.path.join(uchinanchu, "pepgrub", "themes"), os.path.join(uchinanchu, "fusato", "config", "includes.chroot", "boot", "grub", "themes"))
shutil.copytree(os.path.join(uchinanchu, "pepinstaller", "graphics"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "share", "graphics"))
shutil.copytree(os.path.join(uchinanchu, "pepinstaller", "themes"), os.path.join(uchinanchu, "fusato", "config", "includes.installer", "usr", "share", "themes"))

subprocess.run(["lb", "build"], check=True)
