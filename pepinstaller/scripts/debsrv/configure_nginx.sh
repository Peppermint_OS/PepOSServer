#!/bin/bash

# Function to install Nginx if not installed
install_nginx_if_needed() {
    if ! dpkg -l nginx > /dev/null 2>&1; then
        echo "Nginx is not installed. Installing..."
        if sudo apt-get install -y nginx; then
            echo "Nginx installed successfully."
        else
            echo "Failed to install Nginx. Exiting."
            exit 1
        fi
    fi
}

# Function to install Certbot if not installed
install_certbot_if_needed() {
    if ! command -v certbot &> /dev/null; then
        echo "Certbot is not installed. Installing..."
        if sudo apt-get install -y certbot python3-certbot-nginx; then
            echo "Certbot installed successfully."
        else
            echo "Failed to install Certbot. Exiting."
            exit 1
        fi
    fi
}

# Function to start Nginx
start_nginx() {
    sudo systemctl start nginx
    dialog --msgbox "Nginx started." 10 30
}

# Function to stop Nginx
stop_nginx() {
    sudo systemctl stop nginx
    dialog --msgbox "Nginx stopped." 10 30
}

# Function to restart Nginx
restart_nginx() {
    sudo systemctl restart nginx
    dialog --msgbox "Nginx restarted." 10 30
}

# Function to enable Nginx at boot
enable_nginx_at_boot() {
    sudo systemctl enable nginx
    dialog --msgbox "Nginx enabled at boot." 10 30
}

# Function to disable Nginx at boot
disable_nginx_at_boot() {
    sudo systemctl disable nginx
    dialog --msgbox "Nginx disabled at boot." 10 30
}

# Function to secure Nginx installation
secure_nginx() {
    echo "Nginx does not require additional security configuration."
    dialog --msgbox "Nginx installation secured." 10 30
}

# Function to configure Nginx virtual hosts
configure_nginx_virtual_hosts() {
    DOMAIN=$(dialog --inputbox "Enter the domain name for the virtual host (e.g., example.com):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DOMAIN" ]]; then
        sudo mkdir -p /var/www/$DOMAIN/html
        sudo chown -R www-data:www-data /var/www/$DOMAIN/html
        sudo chmod -R 755 /var/www/$DOMAIN
        cat << EOF | sudo tee /etc/nginx/sites-available/$DOMAIN > /dev/null
server {
    listen 80;
    listen [::]:80;

    server_name $DOMAIN;

    root /var/www/$DOMAIN/html;
    index index.html;

    location / {
        try_files \$uri \$uri/ =404;
    }
}
EOF
        sudo ln -s /etc/nginx/sites-available/$DOMAIN /etc/nginx/sites-enabled/
        sudo systemctl reload nginx
        dialog --msgbox "Virtual host for $DOMAIN configured successfully." 10 60
    fi
}

# Function to enable or disable Nginx virtual host (site)
enable_disable_nginx_site() {
    SITE=$(dialog --inputbox "Enter the site configuration file name (without .conf):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -z "$SITE" ]]; then
        dialog --msgbox "No site configuration file name entered. Returning to menu." 10 30
        return
    fi

    ACTION=$(dialog --clear --backtitle "Enable/Disable Nginx Site" \
                    --title "Enable/Disable Nginx Site" \
                    --menu "Choose an action:" 10 40 2 \
                    1 "Enable" \
                    2 "Disable" \
                    3>&1 1>&2 2>&3 3>&-)

    if [ $? -ne 0 ]; then
        return
    fi

    case $ACTION in
        1)
            sudo ln -s /etc/nginx/sites-available/$SITE /etc/nginx/sites-enabled/
            sudo systemctl reload nginx
            dialog --msgbox "Nginx site $SITE enabled." 10 30
            ;;
        2)
            sudo rm -f /etc/nginx/sites-enabled/$SITE
            sudo systemctl reload nginx
            dialog --msgbox "Nginx site $SITE disabled." 10 30
            ;;
        *)
            dialog --msgbox "Invalid option." 10 30
            ;;
    esac
}

# Function to enable or disable Nginx modules
enable_disable_nginx_module() {
    MODULE=$(dialog --inputbox "Enter the name of the Nginx module to enable/disable (e.g., ssl):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -z "$MODULE" ]]; then
        dialog --msgbox "No module name entered. Returning to menu." 10 30
        return
    fi

    ACTION=$(dialog --clear --backtitle "Enable/Disable Nginx Module" \
                    --title "Enable/Disable Nginx Module" \
                    --menu "Choose an action:" 10 40 2 \
                    1 "Enable" \
                    2 "Disable" \
                    3>&1 1>&2 2>&3 3>&-)

    if [ $? -ne 0 ]; then
        return
    fi

    case $ACTION in
        1)
            sudo ln -s /etc/nginx/modules-available/$MODULE.conf /etc/nginx/modules-enabled/
            sudo systemctl restart nginx
            dialog --msgbox "Nginx module $MODULE enabled." 10 30
            ;;
        2)
            sudo rm -f /etc/nginx/modules-enabled/$MODULE.conf
            sudo systemctl restart nginx
            dialog --msgbox "Nginx module $MODULE disabled." 10 30
            ;;
        *)
            dialog --msgbox "Invalid option." 10 30
            ;;
    esac
}

# Function to configure Certbot for Nginx
configure_certbot() {
    DOMAIN=$(dialog --inputbox "Enter the domain name for which you want to configure Certbot (e.g., example.com):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DOMAIN" ]]; then
        sudo certbot --nginx -d $DOMAIN
    fi
}

# Function to configure Nginx
configure_nginx() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure Nginx" \
                        --title "Nginx Menu" \
                        --menu "Choose an option:" 20 60 12 \
                        1 "Install/Check Nginx" \
                        2 "Start Nginx" \
                        3 "Stop Nginx" \
                        4 "Restart Nginx" \
                        5 "Enable Nginx at Boot" \
                        6 "Disable Nginx at Boot" \
                        7 "Secure Nginx Installation" \
                        8 "Configure Virtual Host" \
                        9 "Enable/Disable Nginx Site" \
                        10 "Enable/Disable Nginx Module" \
                        11 "Configure Certbot" \
                        12 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            break
        fi

        clear

        case $CHOICE in
            1) install_nginx_if_needed ;;
            2) start_nginx ;;
            3) stop_nginx ;;
            4) restart_nginx ;;
            5) enable_nginx_at_boot ;;
            6) disable_nginx_at_boot ;;
            7) secure_nginx ;;
            8) configure_nginx_virtual_hosts ;;
            9) enable_disable_nginx_site ;;
            10) enable_disable_nginx_module ;;
            11) configure_certbot ;;
            12) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Show main configuration menu
configure_nginx

