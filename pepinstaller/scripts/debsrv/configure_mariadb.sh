#!/bin/bash

# Function to install MariaDB if not installed
install_mariadb_if_needed() {
    if ! dpkg -l mariadb-server > /dev/null 2>&1; then
        echo "MariaDB is not installed. Installing..."
        if sudo apt-get install -y mariadb-server; then
            echo "MariaDB installed successfully."
        else
            echo "Failed to install MariaDB. Exiting."
            dialog --msgbox "Failed to install MariaDB. Exiting." 10 30
            exit 1
        fi
    else
        dialog --msgbox "MariaDB is already installed." 10 30
    fi
}

# Function to start MariaDB
start_mariadb() {
    sudo systemctl start mariadb
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "MariaDB started successfully." 10 30
    else
        dialog --msgbox "Failed to start MariaDB." 10 30
    fi
}

# Function to stop MariaDB
stop_mariadb() {
    sudo systemctl stop mariadb
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "MariaDB stopped successfully." 10 30
    else
        dialog --msgbox "Failed to stop MariaDB." 10 30
    fi
}

# Function to restart MariaDB
restart_mariadb() {
    sudo systemctl restart mariadb
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "MariaDB restarted successfully." 10 30
    else
        dialog --msgbox "Failed to restart MariaDB." 10 30
    fi
}

# Function to enable MariaDB at boot
enable_mariadb_at_boot() {
    sudo systemctl enable mariadb
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "MariaDB enabled at boot successfully." 10 30
    else
        dialog --msgbox "Failed to enable MariaDB at boot." 10 30
    fi
}

# Function to disable MariaDB at boot
disable_mariadb_at_boot() {
    sudo systemctl disable mariadb
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "MariaDB disabled at boot successfully." 10 30
    else
        dialog --msgbox "Failed to disable MariaDB at boot." 10 30
    fi
}

# Function to secure MariaDB installation
secure_mariadb() {
    sudo mysql_secure_installation
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "MariaDB installation secured successfully." 10 30
    else
        dialog --msgbox "Failed to secure MariaDB installation." 10 30
    fi
}

# Function to create a database
create_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        mysql -e "CREATE DATABASE IF NOT EXISTS $DATABASE;"
        if [[ $? -eq 0 ]]; then
            dialog --msgbox "Database '$DATABASE' created successfully." 10 60
        else
            dialog --msgbox "Failed to create database '$DATABASE'." 10 60
        fi
    fi
}

# Function to create a table
create_table() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        mysql -e "USE $DATABASE; CREATE TABLE IF NOT EXISTS $TABLE (id INT AUTO_INCREMENT PRIMARY KEY);"
        if [[ $? -eq 0 ]]; then
            dialog --msgbox "Table '$TABLE' created in database '$DATABASE' successfully." 10 60
        else
            dialog --msgbox "Failed to create table '$TABLE' in database '$DATABASE'." 10 60
        fi
    fi
}

# Function to insert data into a table
insert_data() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to insert data into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    DATA=$(dialog --inputbox "Enter data to insert into table (e.g., 'value1, value2'):" 10 60 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" && -n "$DATA" ]]; then
        mysql -e "USE $DATABASE; INSERT INTO $TABLE VALUES ($DATA);"
        if [[ $? -eq 0 ]]; then
            dialog --msgbox "Data inserted into table '$TABLE' in database '$DATABASE' successfully." 10 60
        else
            dialog --msgbox "Failed to insert data into table '$TABLE' in database '$DATABASE'." 10 60
        fi
    fi
}

# Function to query data from a table
query_data() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to query from:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        QUERY=$(dialog --inputbox "Enter SQL query (e.g., 'SELECT * FROM $TABLE;'):" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -n "$QUERY" ]]; then
            RESULT=$(mysql -e "USE $DATABASE; $QUERY")
            if [[ $? -eq 0 ]]; then
                dialog --msgbox "Query executed successfully. Result:\n$RESULT" 20 80
            else
                dialog --msgbox "Failed to execute query on table '$TABLE' in database '$DATABASE'." 10 60
            fi
        fi
    fi
}

# Function to backup the database
backup_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to backup:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        mysqldump $DATABASE > $DATABASE.sql
        if [[ $? -eq 0 ]]; then
            dialog --msgbox "Database '$DATABASE' backed up to '$DATABASE.sql' successfully." 10 60
        else
            dialog --msgbox "Failed to backup database '$DATABASE'." 10 60
        fi
    fi
}

# Function to restore the database
restore_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to restore into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        FILE=$(dialog --inputbox "Enter the path to the SQL file to restore:" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -f "$FILE" ]]; then
            mysql $DATABASE < $FILE
            if [[ $? -eq 0 ]]; then
                dialog --msgbox "Database '$DATABASE' restored successfully." 10 60
            else
                dialog --msgbox "Failed to restore database '$DATABASE'." 10 60
            fi
        else
            dialog --msgbox "File not found or invalid." 10 60
        fi
    fi
}

# Function to configure MariaDB
configure_mariadb() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure MariaDB" \
                        --title "MariaDB Menu" \
                        --menu "Choose an option:" 20 60 12 \
                        1 "Install/Check MariaDB" \
                        2 "Start MariaDB" \
                        3 "Stop MariaDB" \
                        4 "Restart MariaDB" \
                        5 "Enable MariaDB at Boot" \
                        6 "Disable MariaDB at Boot" \
                        7 "Secure MariaDB Installation" \
                        8 "Create Database" \
                        9 "Create Table" \
                        10 "Insert Data into Table" \
                        11 "Query Data from Table" \
                        12 "Backup Database" \
                        13 "Restore Database" \
                        14 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            clear
            break
        fi

        clear

        case $CHOICE in
            1) install_mariadb_if_needed ;;
            2) start_mariadb ;;
            3) stop_mariadb ;;
            4) restart_mariadb ;;
            5) enable_mariadb_at_boot ;;
            6) disable_mariadb_at_boot ;;
            7) secure_mariadb ;;
            8) create_database ;;
            9) create_table ;;
            10) insert_data ;;
            11) query_data ;;
            12) backup_database ;;
            13) restore_database ;;
            14) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Show main configuration menu
configure_mariadb

