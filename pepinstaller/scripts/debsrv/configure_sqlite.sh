#!/bin/bash

# Function to install SQLite if not installed
install_sqlite_if_needed() {
    if ! dpkg -l sqlite3 > /dev/null 2>&1; then
        echo "SQLite is not installed. Installing..."
        if sudo apt-get install -y sqlite3; then
            echo "SQLite installed successfully."
        else
            echo "Failed to install SQLite. Exiting."
            exit 1
        fi
    fi
}

# Function to secure SQLite installation
secure_sqlite() {
    echo "SQLite does not require additional security configuration."
    dialog --msgbox "SQLite installation secured." 10 30
}

# Function to create a SQLite database
create_sqlite_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the SQLite database to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        touch $DATABASE.db
        dialog --msgbox "SQLite database '$DATABASE.db' created successfully." 10 60
    fi
}

# Function to create a table in SQLite
create_sqlite_table() {
    DATABASE=$(dialog --inputbox "Enter the name of the SQLite database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to create in SQLite:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        sqlite3 $DATABASE.db "CREATE TABLE $TABLE (id INTEGER PRIMARY KEY);"
        dialog --msgbox "Table '$TABLE' created in SQLite database '$DATABASE.db' successfully." 10 60
    fi
}

# Function to insert data into SQLite table
insert_data_into_sqlite() {
    DATABASE=$(dialog --inputbox "Enter the name of the SQLite database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the SQLite table to insert data into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    DATA=$(dialog --inputbox "Enter data to insert into SQLite table (e.g., 'value1, value2'):" 10 60 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" && -n "$DATA" ]]; then
        sqlite3 $DATABASE.db "INSERT INTO $TABLE VALUES ($DATA);"
        dialog --msgbox "Data inserted into SQLite table '$TABLE' in database '$DATABASE.db' successfully." 10 60
    fi
}

# Function to query data from SQLite table
query_data_from_sqlite() {
    DATABASE=$(dialog --inputbox "Enter the name of the SQLite database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the SQLite table to query from:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        QUERY=$(dialog --inputbox "Enter SQLite query (e.g., 'SELECT * FROM $TABLE;'):" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -n "$QUERY" ]]; then
            sqlite3 $DATABASE.db "$QUERY"
            dialog --msgbox "Query executed successfully." 10 60
        fi
    fi
}

# Function to backup SQLite database
backup_sqlite_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the SQLite database to backup:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        cp $DATABASE.db $DATABASE.backup.db
        dialog --msgbox "SQLite database '$DATABASE.db' backed up to '$DATABASE.backup.db' successfully." 10 60
    fi
}

# Function to restore SQLite database
restore_sqlite_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the SQLite database to restore into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        FILE=$(dialog --inputbox "Enter the path to the SQLite backup file to restore:" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -f "$FILE" ]]; then
            cp $FILE $DATABASE.db
            dialog --msgbox "SQLite database '$DATABASE.db' restored successfully." 10 60
        else
            dialog --msgbox "File not found or invalid." 10 60
        fi
    fi
}

# Function to configure SQLite
configure_sqlite() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure SQLite" \
                        --title "SQLite Menu" \
                        --menu "Choose an option:" 20 60 10 \
                        1 "Install/Check SQLite" \
                        2 "Secure SQLite Installation" \
                        3 "Create SQLite Database" \
                        4 "Create Table in SQLite" \
                        5 "Insert Data into SQLite Table" \
                        6 "Query Data from SQLite Table" \
                        7 "Backup SQLite Database" \
                        8 "Restore SQLite Database" \
                        9 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $CHOICE in
            1) install_sqlite_if_needed ;;
            2) secure_sqlite ;;
            3) create_sqlite_database ;;
            4) create_sqlite_table ;;
            5) insert_data_into_sqlite ;;
            6) query_data_from_sqlite ;;
            7) backup_sqlite_database ;;
            8) restore_sqlite_database ;;
            9) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Show main configuration menu
configure_sqlite

