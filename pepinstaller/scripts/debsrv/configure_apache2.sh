#!/bin/bash

# Function to install Apache if not installed
install_apache_if_needed() {
    if ! dpkg -l apache2 > /dev/null 2>&1; then
        echo "Apache is not installed. Installing..."
        if sudo apt-get install -y apache2; then
            echo "Apache installed successfully."
        else
            echo "Failed to install Apache. Exiting."
            exit 1
        fi
    fi
}

# Function to install Certbot if not installed
install_certbot_if_needed() {
    if ! command -v certbot &> /dev/null; then
        echo "Certbot is not installed. Installing..."
        if sudo apt-get install -y certbot python3-certbot-apache; then
            echo "Certbot installed successfully."
        else
            echo "Failed to install Certbot. Exiting."
            exit 1
        fi
    fi
}

# Function to start Apache
start_apache() {
    sudo systemctl start apache2
    dialog --msgbox "Apache started." 10 30
}

# Function to stop Apache
stop_apache() {
    sudo systemctl stop apache2
    dialog --msgbox "Apache stopped." 10 30
}

# Function to restart Apache
restart_apache() {
    sudo systemctl restart apache2
    dialog --msgbox "Apache restarted." 10 30
}

# Function to enable Apache at boot
enable_apache_at_boot() {
    sudo systemctl enable apache2
    dialog --msgbox "Apache enabled at boot." 10 30
}

# Function to disable Apache at boot
disable_apache_at_boot() {
    sudo systemctl disable apache2
    dialog --msgbox "Apache disabled at boot." 10 30
}

# Function to secure Apache installation
secure_apache() {
    echo "Apache does not require additional security configuration."
    dialog --msgbox "Apache installation secured." 10 30
}

# Function to configure Apache virtual hosts
configure_apache_virtual_hosts() {
    DOMAIN=$(dialog --inputbox "Enter the domain name for the virtual host (e.g., example.com):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [ $? -ne 0 ]; then
        return
    fi
    if [[ -n "$DOMAIN" ]]; then
        sudo mkdir -p /var/www/$DOMAIN/public_html
        sudo chown -R www-data:www-data /var/www/$DOMAIN/public_html
        sudo chmod -R 755 /var/www/$DOMAIN
        cat << EOF | sudo tee /etc/apache2/sites-available/$DOMAIN.conf > /dev/null
<VirtualHost *:80>
    ServerAdmin webmaster@$DOMAIN
    ServerName $DOMAIN
    DocumentRoot /var/www/$DOMAIN/public_html
    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF
        sudo a2ensite $DOMAIN.conf
        sudo systemctl reload apache2
        dialog --msgbox "Virtual host for $DOMAIN configured successfully." 10 60
    fi
}

# Function to enable or disable Apache virtual host (site)
enable_disable_apache_site() {
    SITE=$(dialog --inputbox "Enter the site configuration file name (without .conf):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [ $? -ne 0 ]; then
        return
    fi
    if [[ -z "$SITE" ]]; then
        dialog --msgbox "No site configuration file name entered. Returning to menu." 10 30
        return
    fi

    ACTION=$(dialog --clear --backtitle "Enable/Disable Apache Site" \
                    --title "Enable/Disable Apache Site" \
                    --menu "Choose an action:" 10 40 2 \
                    1 "Enable" \
                    2 "Disable" \
                    3>&1 1>&2 2>&3 3>&-)

    if [ $? -ne 0 ]; then
        return
    fi

    case $ACTION in
        1)
            sudo a2ensite $SITE.conf
            sudo systemctl reload apache2
            dialog --msgbox "Apache site $SITE enabled." 10 30
            ;;
        2)
            sudo a2dissite $SITE.conf
            sudo systemctl reload apache2
            dialog --msgbox "Apache site $SITE disabled." 10 30
            ;;
        *)
            dialog --msgbox "Invalid option." 10 30
            ;;
    esac
}

# Function to enable or disable Apache modules
enable_disable_apache_module() {
    MODULE=$(dialog --inputbox "Enter the name of the Apache module to enable/disable (e.g., rewrite):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [ $? -ne 0 ]; then
        return
    fi
    if [[ -z "$MODULE" ]]; then
        dialog --msgbox "No module name entered. Returning to menu." 10 30
        return
    fi

    ACTION=$(dialog --clear --backtitle "Enable/Disable Apache Module" \
                    --title "Enable/Disable Apache Module" \
                    --menu "Choose an action:" 10 40 2 \
                    1 "Enable" \
                    2 "Disable" \
                    3>&1 1>&2 2>&3 3>&-)

    if [ $? -ne 0 ]; then
        return
    fi

    case $ACTION in
        1)
            sudo a2enmod $MODULE
            sudo systemctl restart apache2
            dialog --msgbox "Apache module $MODULE enabled." 10 30
            ;;
        2)
            sudo a2dismod $MODULE
            sudo systemctl restart apache2
            dialog --msgbox "Apache module $MODULE disabled." 10 30
            ;;
        *)
            dialog --msgbox "Invalid option." 10 30
            ;;
    esac
}

# Function to configure Certbot for Apache
configure_certbot() {
    DOMAIN=$(dialog --inputbox "Enter the domain name for which you want to configure Certbot (e.g., example.com):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [ $? -ne 0 ]; then
        return
    fi
    if [[ -n "$DOMAIN" ]]; then
        sudo certbot --apache -d $DOMAIN
    fi
}

# Function to configure Apache
configure_apache() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure Apache" \
                        --title "Apache Menu" \
                        --menu "Choose an option:" 20 60 12 \
                        1 "Install/Check Apache" \
                        2 "Start Apache" \
                        3 "Stop Apache" \
                        4 "Restart Apache" \
                        5 "Enable Apache at Boot" \
                        6 "Disable Apache at Boot" \
                        7 "Secure Apache Installation" \
                        8 "Configure Virtual Host" \
                        9 "Enable/Disable Apache Site" \
                        10 "Enable/Disable Apache Module" \
                        11 "Configure Certbot" \
                        12 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            break
        fi

        clear

        case $CHOICE in
            1) install_apache_if_needed ;;
            2) start_apache ;;
            3) stop_apache ;;
            4) restart_apache ;;
            5) enable_apache_at_boot ;;
            6) disable_apache_at_boot ;;
            7) secure_apache ;;
            8) configure_apache_virtual_hosts ;;
            9) enable_disable_apache_site ;;
            10) enable_disable_apache_module ;;
            11) configure_certbot ;;
            12) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Show main configuration menu
configure_apache

