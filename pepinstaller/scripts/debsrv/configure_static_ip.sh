#!/bin/bash

# Function to configure static IP
configure_static_ip() {
    while true; do
        # Prompt for network interface
        INTERFACE=$(dialog --inputbox "Enter physical network interface (e.g., eth0):" 10 60 3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            return 1
        fi

        # Prompt for IP address
        IP_ADDRESS=$(dialog --inputbox "Enter static IP address (e.g., 192.168.1.100):" 10 60 3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            return 1
        fi

        # Validate IP address format
        if ! valid_ip "$IP_ADDRESS"; then
            dialog --msgbox "Invalid IP address format. Please enter a valid IP address." 10 60
            continue
        fi

        # Prompt for netmask
        NETMASK=$(dialog --inputbox "Enter netmask (e.g., 255.255.255.0):" 10 60 3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            return 1
        fi

        # Validate netmask format
        if ! valid_netmask "$NETMASK"; then
            dialog --msgbox "Invalid netmask format. Please enter a valid netmask." 10 60
            continue
        fi

        # Prompt for gateway
        GATEWAY=$(dialog --inputbox "Enter gateway (optional, leave blank if none):" 10 60 3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            return 1
        fi

        # Prompt for DNS server
        DNS_SERVER=$(dialog --inputbox "Enter DNS server (optional, leave blank if none):" 10 60 3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            return 1
        fi

        # Apply the static IP configuration
        if [ -z "$INTERFACE" ] || [ -z "$IP_ADDRESS" ] || [ -z "$NETMASK" ]; then
            dialog --msgbox "Network interface, IP address, and netmask cannot be empty. Please enter valid information." 10 60
        else
            apply_static_ip "$INTERFACE" "$IP_ADDRESS" "$NETMASK" "$GATEWAY" "$DNS_SERVER"
            return $?
        fi
    done
}

# Function to validate IP address format
valid_ip() {
    local ip=$1
    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        local IFS='.'
        ip=($ip)
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
    else
        false
    fi
}

# Function to validate netmask format
valid_netmask() {
    local netmask=$1
    if [[ $netmask =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        local IFS='.'
        netmask=($netmask)
        [[ ${netmask[0]} -le 255 && ${netmask[1]} -le 255 && ${netmask[2]} -le 255 && ${netmask[3]} -le 255 ]]
    else
        false
    fi
}

# Function to apply static IP configuration
apply_static_ip() {
    local interface=$1
    local ip_address=$2
    local netmask=$3
    local gateway=$4
    local dns_server=$5

    # Apply configuration to /etc/network/interfaces
    cat <<EOF | sudo tee /etc/network/interfaces >/dev/null
auto lo
iface lo inet loopback

auto $interface
iface $interface inet static
    address $ip_address
    netmask $netmask
    gateway $gateway
    dns-nameservers $dns_server
EOF

    # Restart networking service
    sudo systemctl restart networking

    # Check if configuration applied successfully
    if ifconfig $interface | grep -q "$ip_address"; then
        dialog --msgbox "Static IP configuration applied successfully:\nInterface: $interface\nIP Address: $ip_address\nNetmask: $netmask\nGateway: $gateway\nDNS Server: $dns_server" 12 60
        return 0
    else
        dialog --msgbox "Failed to apply static IP configuration. Please check your settings and try again." 10 60
        return 1
    fi
}

# Main function to configure host settings
configure_host() {
    while true; do
        HOST_CHOICE=$(dialog --clear --backtitle "Host Configuration" \
                            --title "Host Menu" \
                            --menu "Choose an option:" 20 60 10 \
                            1 "Configure Static IP" \
                            2 "Show Current Network Settings" \
                            3 "Back to Main Menu" \
                            3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            break
        fi

        case $HOST_CHOICE in
            1) configure_static_ip ;;
            2) show_current_network_settings ;;
            3) break ;;
            *) dialog --msgbox "Invalid option. Please choose a valid option." 10 60 ;;
        esac
    done
}

# Function to show current network settings
show_current_network_settings() {
    CURRENT_SETTINGS=$(ip addr show)
    dialog --msgbox "Current Network Settings:\n\n$CURRENT_SETTINGS" 20 80
}

# Execute the main function to configure host settings
configure_host
