#!/bin/bash

# Function to update package list
update_package_list() {
    echo "Updating package list..."
    if ! sudo apt update; then
        echo "Failed to update package list."
        dialog --title "Error" --msgbox "Failed to update package list. Check your internet connection or repository settings." 10 60
        return 1
    fi
    echo "Package list updated successfully."
}

# Function to install selected packages
install_selected_packages() {
    local selected_packages=("$@")

    echo "Installing selected packages..."
    for package in "${selected_packages[@]}"; do
        echo "Installing package: $package"
        if sudo apt install -y "$package"; then
            echo "Package $package installed successfully."
        else
            echo "Failed to install package: $package"
            dialog --title "Error" --msgbox "Failed to install package: $package" 10 60
        fi
    done
    echo "All packages installed successfully."
    dialog --title "Success" --msgbox "All packages installed successfully." 10 60
}

# List of packages available for installation
PACKAGES=("vim"
          "nano"
          "emacs"
          "mcedit"
          "joe"
          "wget"
          "curl"
          "lynx"
          "htop"
          "iftop"
          "iotop"
          "net-tools"
          "dnsutils")

# Check if script is running as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    dialog --title "Error" --msgbox "This script must be run as root." 10 30
    exit 1
fi

# Update package list before installation
update_package_list

# Prepare package list for dialog
DIALOG_PACKAGES=()
for idx in "${!PACKAGES[@]}"; do
    DIALOG_PACKAGES+=("$((idx + 1))" "${PACKAGES[$idx]}" off)
done

while true; do
    # Show dialog box for package selection
    echo "Starting package selection dialog..."
    selections=$(dialog --stdout --checklist "Select packages to install (use space to select):" 20 60 ${#PACKAGES[@]} "${DIALOG_PACKAGES[@]}" 3>&1 1>&2 2>&3)

    # Check if cancel button is pressed or no selection was made
    if [[ $? -ne 0 ]]; then
        echo "Installation canceled."
        dialog --title "Information" --msgbox "Installation canceled." 10 60
        exit 0
    fi

    # Check if no selection was made
    if [[ -z "$selections" ]]; then
        echo "No packages selected. Please select at least one package."
        dialog --title "Error" --msgbox "No packages selected. Please select at least one package." 10 60
    else
        break
    fi
done

# Convert selection indices into package names
packages_to_install=()
for index in $selections; do
    packages_to_install+=("${PACKAGES[$((index - 1))]}")
done

# Call function to install selected packages
install_selected_packages "${packages_to_install[@]}"

echo "Script completed."

