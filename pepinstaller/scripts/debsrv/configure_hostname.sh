#!/bin/bash

# Function to set hostname
set_hostname() {
    NEW_HOSTNAME=$(dialog --inputbox "Enter the new hostname:" 10 60 3>&1 1>&2 2>&3 3>&-)

    # Check if user canceled
    if [ $? -ne 0 ]; then
        return 1
    fi

    if [ -z "$NEW_HOSTNAME" ]; then
        dialog --msgbox "Hostname cannot be empty. Please enter a valid hostname." 10 60
    else
        hostnamectl set-hostname "$NEW_HOSTNAME"
        dialog --msgbox "Hostname set to $NEW_HOSTNAME." 10 60
    fi
}

# Function to display current hostname
show_hostname() {
    CURRENT_HOSTNAME=$(hostnamectl --static)
    dialog --msgbox "Current hostname is: $CURRENT_HOSTNAME" 10 60
}

# Function to display network interfaces
show_network_interfaces() {
    dialog --msgbox "$(ip -o link show | awk '{print $2,$9}')" 20 60
}

# Function to display IP addresses
show_ip_addresses() {
    dialog --msgbox "$(ip -4 addr show | grep inet)" 30 80
}

# Function to configure DNS settings
configure_dns() {
    while true; do
        DNS_CHOICE=$(dialog --clear --backtitle "DNS Configuration" \
                            --title "DNS Menu" \
                            --menu "Choose an option:" 20 60 10 \
                            1 "Set DNS Servers" \
                            2 "Show DNS Configuration" \
                            3 "Back to Main Menu" \
                            3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            break
        fi

        case $DNS_CHOICE in
            1) set_dns_servers ;;
            2) show_dns_configuration ;;
            3) break ;;
            *) dialog --msgbox "Invalid option. Please choose a valid option." 10 60 ;;
        esac
    done
}

# Function to set DNS servers
set_dns_servers() {
    DNS_SERVERS=$(dialog --inputbox "Enter DNS servers separated by comma (e.g., 8.8.8.8,8.8.4.4):" 10 60 3>&1 1>&2 2>&3 3>&-)

    # Check if user canceled
    if [ $? -ne 0 ]; then
        return 1
    fi

    if [ -z "$DNS_SERVERS" ]; then
        dialog --msgbox "DNS servers cannot be empty. Please enter valid DNS servers." 10 60
    else
        echo "nameserver $DNS_SERVERS" | sudo tee /etc/resolv.conf >/dev/null
        dialog --msgbox "DNS servers set to: $DNS_SERVERS" 10 60
    fi
}

# Function to show current DNS configuration
show_dns_configuration() {
    CURRENT_DNS=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}' | tr '\n' ' ')
    dialog --msgbox "Current DNS configuration:\n$CURRENT_DNS" 10 60
}

# Function to configure host options
configure_host() {
    while true; do
        HOST_CHOICE=$(dialog --clear --backtitle "Host Configuration" \
                            --title "Host Menu" \
                            --menu "Choose an option:" 20 60 10 \
                            1 "Set Hostname" \
                            2 "Show Hostname" \
                            3 "Show Network Interfaces" \
                            4 "Show IP Addresses" \
                            5 "DNS Configuration" \
                            6 "Back to Main Menu" \
                            3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            break
        fi

        case $HOST_CHOICE in
            1) set_hostname ;;
            2) show_hostname ;;
            3) show_network_interfaces ;;
            4) show_ip_addresses ;;
            5) configure_dns ;;
            6) break ;;
            *) dialog --msgbox "Invalid option. Please choose a valid option." 10 60 ;;
        esac
    done
}

# Execute the main function to configure host settings
configure_host
