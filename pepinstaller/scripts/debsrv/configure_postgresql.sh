#!/bin/bash

# Função para instalar PostgreSQL, se necessário
install_postgresql_if_needed() {
    if ! dpkg -l postgresql > /dev/null 2>&1; then
        echo "PostgreSQL não está instalado. Instalando..."
        if sudo apt-get install -y postgresql; then
            echo "PostgreSQL instalado com sucesso."
            dialog --msgbox "PostgreSQL instalado com sucesso." 10 30
        else
            echo "Falha ao instalar PostgreSQL. Saindo."
            dialog --msgbox "Falha ao instalar PostgreSQL. Saindo." 10 30
            exit 1
        fi
    else
        dialog --msgbox "PostgreSQL já está instalado." 10 30
    fi
}

# Função para iniciar PostgreSQL
start_postgresql() {
    sudo systemctl start postgresql
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "PostgreSQL iniciado com sucesso." 10 30
    else
        dialog --msgbox "Falha ao iniciar PostgreSQL." 10 30
    fi
}

# Função para parar PostgreSQL
stop_postgresql() {
    sudo systemctl stop postgresql
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "PostgreSQL parado com sucesso." 10 30
    else
        dialog --msgbox "Falha ao parar PostgreSQL." 10 30
    fi
}

# Função para reiniciar PostgreSQL
restart_postgresql() {
    sudo systemctl restart postgresql
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "PostgreSQL reiniciado com sucesso." 10 30
    else
        dialog --msgbox "Falha ao reiniciar PostgreSQL." 10 30
    fi
}

# Função para habilitar PostgreSQL na inicialização
enable_postgresql_at_boot() {
    sudo systemctl enable postgresql
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "PostgreSQL habilitado na inicialização com sucesso." 10 30
    else
        dialog --msgbox "Falha ao habilitar PostgreSQL na inicialização." 10 30
    fi
}

# Função para desabilitar PostgreSQL na inicialização
disable_postgresql_at_boot() {
    sudo systemctl disable postgresql
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "PostgreSQL desabilitado na inicialização com sucesso." 10 30
    else
        dialog --msgbox "Falha ao desabilitar PostgreSQL na inicialização." 10 30
    fi
}

# Função para garantir a instalação do PostgreSQL
secure_postgresql() {
    sudo passwd postgres
    sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'your_password';"
    if [[ $? -eq 0 ]]; then
        dialog --msgbox "Instalação do PostgreSQL protegida com sucesso." 10 30
    else
        dialog --msgbox "Falha ao proteger a instalação do PostgreSQL." 10 30
    fi
}

# Função para criar um banco de dados
create_database() {
    DATABASE=$(dialog --inputbox "Digite o nome do banco de dados a ser criado:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        sudo -u postgres createdb $DATABASE
        if [[ $? -eq 0 ]]; então
            dialog --msgbox "Banco de dados '$DATABASE' criado com sucesso." 10 60
        else
            dialog --msgbox "Falha ao criar o banco de dados '$DATABASE'." 10 60
        fi
    fi
}

# Função para criar uma tabela
create_table() {
    DATABASE=$(dialog --inputbox "Digite o nome do banco de dados:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Digite o nome da tabela a ser criada:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; então
        sudo -u postgres psql -d $DATABASE -c "CREATE TABLE $TABLE (id SERIAL PRIMARY KEY);"
        if [[ $? -eq 0 ]]; então
            dialog --msgbox "Tabela '$TABLE' criada no banco de dados '$DATABASE' com sucesso." 10 60
        else
            dialog --msgbox "Falha ao criar a tabela '$TABLE' no banco de dados '$DATABASE'." 10 60
        fi
    fi
}

# Função para inserir dados em uma tabela
insert_data() {
    DATABASE=$(dialog --inputbox "Digite o nome do banco de dados:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Digite o nome da tabela para inserir dados:" 10 40 3>&1 1>&2 2>&3 3>&-)
    DATA=$(dialog --inputbox "Digite os dados para inserir na tabela (por exemplo, 'valor1, valor2'):" 10 60 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" && -n "$DATA" ]]; então
        sudo -u postgres psql -d $DATABASE -c "INSERT INTO $TABLE VALUES ($DATA);"
        if [[ $? -eq 0 ]]; então
            dialog --msgbox "Dados inseridos na tabela '$TABLE' no banco de dados '$DATABASE' com sucesso." 10 60
        else
            dialog --msgbox "Falha ao inserir dados na tabela '$TABLE' no banco de dados '$DATABASE'." 10 60
        fi
    fi
}

# Função para consultar dados de uma tabela
query_data() {
    DATABASE=$(dialog --inputbox "Digite o nome do banco de dados:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Digite o nome da tabela para consultar:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; então
        QUERY=$(dialog --inputbox "Digite a consulta SQL (por exemplo, 'SELECT * FROM $TABLE;'):" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -n "$QUERY" ]]; então
            RESULT=$(sudo -u postgres psql -d $DATABASE -c "$QUERY")
            if [[ $? -eq 0 ]]; então
                dialog --msgbox "Consulta executada com sucesso. Resultado:\n$RESULT" 20 80
            else
                dialog --msgbox "Falha ao executar a consulta na tabela '$TABLE' no banco de dados '$DATABASE'." 10 60
            fi
        fi
    fi
}

# Função para fazer backup do banco de dados
backup_database() {
    DATABASE=$(dialog --inputbox "Digite o nome do banco de dados para fazer backup:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; então
        sudo -u postgres pg_dump $DATABASE > $DATABASE.sql
        if [[ $? -eq 0 ]]; então
            dialog --msgbox "Banco de dados '$DATABASE' feito backup para '$DATABASE.sql' com sucesso." 10 60
        else
            dialog --msgbox "Falha ao fazer backup do banco de dados '$DATABASE'." 10 60
        fi
    fi
}

# Função para restaurar o banco de dados
restore_database() {
    DATABASE=$(dialog --inputbox "Digite o nome do banco de dados para restaurar:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; então
        FILE=$(dialog --inputbox "Digite o caminho para o arquivo SQL para restaurar:" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -f "$FILE" ]]; então
            sudo -u postgres psql -d $DATABASE < $FILE
            if [[ $? -eq 0 ]]; então
                dialog --msgbox "Banco de dados '$DATABASE' restaurado com sucesso." 10 60
            else
                dialog --msgbox "Falha ao restaurar o banco de dados '$DATABASE'." 10 60
            fi
        else
            dialog --msgbox "Arquivo não encontrado ou inválido." 10 60
        fi
    fi
}

# Função para configurar PostgreSQL
configure_postgresql() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configurar PostgreSQL" \
                        --title "Menu do PostgreSQL" \
                        --menu "Escolha uma opção:" 20 60 14 \
                        1 "Instalar/Verificar PostgreSQL" \
                        2 "Iniciar PostgreSQL" \
                        3 "Parar PostgreSQL" \
                        4 "Reiniciar PostgreSQL" \
                        5 "Habilitar PostgreSQL na Inicialização" \
                        6 "Desabilitar PostgreSQL na Inicialização" \
                        7 "Proteger Instalação do PostgreSQL" \
                        8 "Criar Banco de Dados" \
                        9 "Criar Tabela" \
                        10 "Inserir Dados na Tabela" \
                        11 "Consultar Dados da Tabela" \
                        12 "Fazer Backup do Banco de Dados" \
                        13 "Restaurar Banco de Dados" \
                        14 "Retornar ao Menu Principal" \
                        3>&1 1>&2 2>&3 3>&-)

        clear

        # Verificar se o usuário cancelou
        if [ $? -eq 1 ]; então
            break
        fi

        case $CHOICE in
            1) install_postgresql_if_needed ;;
            2) start_postgresql ;;
            3) stop_postgresql ;;
            4) restart_postgresql ;;
            5) enable_postgresql_at_boot ;;
            6) disable_postgresql_at_boot ;;
            7) secure_postgresql ;;
            8) create_database ;;
            9) create_table ;;
            10) insert_data ;;
            11) query_data ;;
            12) backup_database ;;
            13) restore_database ;;
            14) break ;;
            *) dialog --msgbox "Opção inválida." 10 30 ;;
        esac
    done
}

# Mostrar menu de configuração principal
configure_postgresql

