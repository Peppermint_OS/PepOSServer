#!/bin/bash

# Function to install Postfix if not installed
install_postfix_if_needed() {
    if ! dpkg -l postfix > /dev/null 2>&1; then
        echo "Postfix is not installed. Installing..."
        if sudo apt-get install -y postfix; then
            echo "Postfix installed successfully."
        else
            echo "Failed to install Postfix. Exiting."
            exit 1
        fi
    fi
}

# Function to configure Postfix interactively
configure_postfix() {
    echo "Configuring Postfix..."
    if ! sudo dpkg-reconfigure postfix; then
        echo "Failed to configure Postfix."
        dialog --title "Error" --msgbox "Failed to configure Postfix." 10 60
        exit 1
    fi
    echo "Postfix configured successfully."
    dialog --title "Success" --msgbox "Postfix configured successfully." 10 60
}

# Function to start Postfix service
start_postfix() {
    echo "Starting Postfix service..."
    if ! sudo systemctl start postfix; then
        echo "Failed to start Postfix service."
        dialog --title "Error" --msgbox "Failed to start Postfix service." 10 60
        exit 1
    fi
    echo "Postfix service started successfully."
    dialog --title "Success" --msgbox "Postfix service started successfully." 10 60
}

# Function to stop Postfix service
stop_postfix() {
    echo "Stopping Postfix service..."
    if ! sudo systemctl stop postfix; then
        echo "Failed to stop Postfix service."
        dialog --title "Error" --msgbox "Failed to stop Postfix service." 10 60
        exit 1
    fi
    echo "Postfix service stopped successfully."
    dialog --title "Success" --msgbox "Postfix service stopped successfully." 10 60
}

# Function to restart Postfix service
restart_postfix() {
    echo "Restarting Postfix service..."
    if ! sudo systemctl restart postfix; then
        echo "Failed to restart Postfix service."
        dialog --title "Error" --msgbox "Failed to restart Postfix service." 10 60
        exit 1
    fi
    echo "Postfix service restarted successfully."
    dialog --title "Success" --msgbox "Postfix service restarted successfully." 10 60
}

# Function to enable Postfix service at boot
enable_postfix_at_boot() {
    echo "Enabling Postfix service at boot..."
    if ! sudo systemctl enable postfix; then
        echo "Failed to enable Postfix service at boot."
        dialog --title "Error" --msgbox "Failed to enable Postfix service at boot." 10 60
        exit 1
    fi
    echo "Postfix service enabled at boot successfully."
    dialog --title "Success" --msgbox "Postfix service enabled at boot successfully." 10 60
}

# Function to disable Postfix service at boot
disable_postfix_at_boot() {
    echo "Disabling Postfix service at boot..."
    if ! sudo systemctl disable postfix; then
        echo "Failed to disable Postfix service at boot."
        dialog --title "Error" --msgbox "Failed to disable Postfix service at boot." 10 60
        exit 1
    fi
    echo "Postfix service disabled at boot successfully."
    dialog --title "Success" --msgbox "Postfix service disabled at boot successfully." 10 60
}

# Function to configure Postfix securely (optional)
secure_postfix() {
    echo "Securing Postfix configuration..."
    # You can add additional secure configuration steps here if needed
    echo "Postfix configuration secured successfully."
    dialog --title "Success" --msgbox "Postfix configuration secured successfully." 10 60
}

# Function to display the main menu
main_menu() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Postfix Configuration" \
                        --title "Postfix Menu" \
                        --menu "Choose an option:" 15 60 9 \
                        1 "Install/Check Postfix" \
                        2 "Configure Postfix" \
                        3 "Start Postfix" \
                        4 "Stop Postfix" \
                        5 "Restart Postfix" \
                        6 "Enable Postfix at Boot" \
                        7 "Disable Postfix at Boot" \
                        8 "Secure Postfix Configuration" \
                        9 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -ne 0 ]; then
            break
        fi

        clear

        case $CHOICE in
            1) install_postfix_if_needed ;;
            2) configure_postfix ;;
            3) start_postfix ;;
            4) stop_postfix ;;
            5) restart_postfix ;;
            6) enable_postfix_at_boot ;;
            7) disable_postfix_at_boot ;;
            8) secure_postfix ;;
            9) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done

    echo "Postfix configuration script completed."
}

# Display main menu
main_menu

