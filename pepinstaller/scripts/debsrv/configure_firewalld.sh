k#!/bin/bash

# Function to enable firewalld
enable_firewalld() {
    systemctl enable firewalld
    systemctl start firewalld
    dialog --msgbox "firewalld enabled and started." 10 30
}

# Function to disable firewalld
disable_firewalld() {
    systemctl stop firewalld
    systemctl disable firewalld
    dialog --msgbox "firewalld disabled and stopped." 10 30
}

# Function to add service to firewalld
add_service() {
    SERVICE=$(dialog --inputbox "Enter the service name to add (e.g., ssh, http, https):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$SERVICE" ]; then
        dialog --msgbox "Please enter a valid service name." 10 30
    else
        firewall-cmd --permanent --add-service=$SERVICE
        firewall-cmd --reload
        dialog --msgbox "Service $SERVICE added to firewalld." 10 30
    fi
}

# Function to remove service from firewalld
remove_service() {
    SERVICE=$(dialog --inputbox "Enter the service name to remove:" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$SERVICE" ]; then
        dialog --msgbox "Please enter a valid service name." 10 30
    else
        firewall-cmd --permanent --remove-service=$SERVICE
        firewall-cmd --reload
        dialog --msgbox "Service $SERVICE removed from firewalld." 10 30
    fi
}

# Function to add port to firewalld
add_port() {
    PORT=$(dialog --inputbox "Enter the port to add (e.g., 8080/tcp):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$PORT" ]; then
        dialog --msgbox "Please enter a valid port." 10 30
    else
        firewall-cmd --permanent --add-port=$PORT
        firewall-cmd --reload
        dialog --msgbox "Port $PORT added to firewalld." 10 30
    fi
}

# Function to remove port from firewalld
remove_port() {
    PORT=$(dialog --inputbox "Enter the port to remove (e.g., 8080/tcp):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$PORT" ]; then
        dialog --msgbox "Please enter a valid port." 10 30
    else
        firewall-cmd --permanent --remove-port=$PORT
        firewall-cmd --reload
        dialog --msgbox "Port $PORT removed from firewalld." 10 30
    fi
}

# Function to configure zones in firewalld
configure_zones() {
    ZONE=$(dialog --inputbox "Enter the zone name (e.g., public, home, work):" 10 50 3>&1 1>&2 2>&3 3>&-)
    INTERFACE=$(dialog --inputbox "Enter the interface name (e.g., eth0, wlan0):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$ZONE" ] || [ -z "$INTERFACE" ]; then
        dialog --msgbox "Please enter valid zone and interface names." 10 30
    else
        firewall-cmd --zone=$ZONE --add-interface=$INTERFACE --permanent
        firewall-cmd --reload
        dialog --msgbox "Interface $INTERFACE added to zone $ZONE." 10 30
    fi
}

# Function to configure firewalld options
configure_firewalld() {
    while true; do
        FIREWALL_CHOICE=$(dialog --clear --backtitle "Firewalld Configuration" \
                                --title "Firewalld Menu" \
                                --menu "Choose an option:" 20 60 10 \
                                1 "Enable firewalld" \
                                2 "Disable firewalld" \
                                3 "Add Service" \
                                4 "Remove Service" \
                                5 "Add Port" \
                                6 "Remove Port" \
                                7 "Configure Zones" \
                                8 "Back to Main Menu" \
                                3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $FIREWALL_CHOICE in
            1) enable_firewalld ;;
            2) disable_firewalld ;;
            3) add_service ;;
            4) remove_service ;;
            5) add_port ;;
            6) remove_port ;;
            7) configure_zones ;;
            8) break ;;
            *) dialog --msgbox "Invalid option. Please choose a valid option." 10 30 ;;
        esac
    done
}

# Execute the main function to configure firewalld
configure_firewalld

