#!/bin/bash

# Detect init system
detect_init_system() {
    if [[ -x "$(command -v systemctl)" ]]; then
        INIT_SYSTEM="systemd"
    elif [[ -x "$(command -v service)" ]]; then
        INIT_SYSTEM="sysvinit"
    elif [[ -x "$(command -v rc-service)" ]]; then
        INIT_SYSTEM="openrc"
    elif [[ -x "$(command -v runsvdir)" ]]; then
        INIT_SYSTEM="runit"
    else
        echo "Unsupported init system. Exiting."
        exit 1
    fi
}

# Function to install PostgreSQL if not installed
install_postgresql_if_needed() {
    case $INIT_SYSTEM in
        systemd | sysvinit | openrc | runit)
            if ! dpkg -l postgresql > /dev/null 2>&1; then
                echo "PostgreSQL is not installed. Installing..."
                if sudo apt-get install -y postgresql; then
                    echo "PostgreSQL installed successfully."
                else
                    echo "Failed to install PostgreSQL. Exiting."
                    exit 1
                fi
            fi
            ;;
        *)
            echo "Unsupported init system. Cannot install PostgreSQL."
            ;;
    esac
}

# Function to start PostgreSQL
start_postgresql() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Starting PostgreSQL..."
            sudo systemctl start postgresql
            dialog --msgbox "PostgreSQL started." 10 30
            ;;
        openrc)
            sudo rc-service postgresql start
            dialog --msgbox "PostgreSQL started." 10 30
            ;;
        runit)
            echo "Runit does not manage PostgreSQL."
            ;;
    esac
}

# Function to stop PostgreSQL
stop_postgresql() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Stopping PostgreSQL..."
            sudo systemctl stop postgresql
            dialog --msgbox "PostgreSQL stopped." 10 30
            ;;
        openrc)
            sudo rc-service postgresql stop
            dialog --msgbox "PostgreSQL stopped." 10 30
            ;;
        runit)
            echo "Runit does not manage PostgreSQL."
            ;;
    esac
}

# Function to restart PostgreSQL
restart_postgresql() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Restarting PostgreSQL..."
            sudo systemctl restart postgresql
            dialog --msgbox "PostgreSQL restarted." 10 30
            ;;
        openrc)
            sudo rc-service postgresql restart
            dialog --msgbox "PostgreSQL restarted." 10 30
            ;;
        runit)
            echo "Runit does not manage PostgreSQL."
            ;;
    esac
}

# Function to enable PostgreSQL at boot
enable_postgresql_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Enabling PostgreSQL at boot..."
            sudo systemctl enable postgresql
            dialog --msgbox "PostgreSQL enabled at boot." 10 30
            ;;
        openrc)
            sudo rc-update add postgresql default
            dialog --msgbox "PostgreSQL enabled at boot." 10 30
            ;;
        runit)
            echo "Runit does not manage PostgreSQL."
            ;;
    esac
}

# Function to disable PostgreSQL at boot
disable_postgresql_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Disabling PostgreSQL at boot..."
            sudo systemctl disable postgresql
            dialog --msgbox "PostgreSQL disabled at boot." 10 30
            ;;
        openrc)
            sudo rc-update del postgresql default
            dialog --msgbox "PostgreSQL disabled at boot." 10 30
            ;;
        runit)
            echo "Runit does not manage PostgreSQL."
            ;;
    esac
}

# Function to secure PostgreSQL installation
secure_postgresql() {
    echo "Securing PostgreSQL installation..."
    sudo passwd postgres
    sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'your_password';"
    dialog --msgbox "PostgreSQL installation secured." 10 30
}

# Function to create a database
create_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        sudo -u postgres createdb $DATABASE
        dialog --msgbox "Database '$DATABASE' created successfully." 10 60
    fi
}

# Function to create a table
create_table() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        sudo -u postgres psql -d $DATABASE -c "CREATE TABLE $TABLE (id SERIAL PRIMARY KEY);"
        dialog --msgbox "Table '$TABLE' created in database '$DATABASE' successfully." 10 60
    fi
}

# Function to insert data into a table
insert_data() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to insert data into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    DATA=$(dialog --inputbox "Enter data to insert into table (e.g., 'value1, value2'):" 10 60 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" && -n "$DATA" ]]; then
        sudo -u postgres psql -d $DATABASE -c "INSERT INTO $TABLE VALUES ($DATA);"
        dialog --msgbox "Data inserted into table '$TABLE' in database '$DATABASE' successfully." 10 60
    fi
}

# Function to query data from a table
query_data() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to query from:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        QUERY=$(dialog --inputbox "Enter SQL query (e.g., 'SELECT * FROM $TABLE;'):" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -n "$QUERY" ]]; then
            sudo -u postgres psql -d $DATABASE -c "$QUERY"
            dialog --msgbox "Query executed successfully." 10 60
        fi
    fi
}

# Function to backup the database
backup_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to backup:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        sudo -u postgres pg_dump $DATABASE > $DATABASE.sql
        dialog --msgbox "Database '$DATABASE' backed up to '$DATABASE.sql' successfully." 10 60
    fi
}

# Function to restore the database
restore_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to restore into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        FILE=$(dialog --inputbox "Enter the path to the SQL file to restore:" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -f "$FILE" ]]; then
            sudo -u postgres psql -d $DATABASE < $FILE
            dialog --msgbox "Database '$DATABASE' restored successfully." 10 60
        else
            dialog --msgbox "File not found or invalid." 10 60
        fi
    fi
}

# Function to configure PostgreSQL
configure_postgresql() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure PostgreSQL" \
                        --title "PostgreSQL Menu" \
                        --menu "Choose an option:" 20 60 14 \
                        1 "Install/Check PostgreSQL" \
                        2 "Start PostgreSQL" \
                        3 "Stop PostgreSQL" \
                        4 "Restart PostgreSQL" \
                        5 "Enable PostgreSQL at Boot" \
                        6 "Disable PostgreSQL at Boot" \
                        7 "Secure PostgreSQL Installation" \
                        8 "Create Database" \
                        9 "Create Table" \
                        10 "Insert Data into Table" \
                        11 "Query Data from Table" \
                        12 "Backup Database" \
                        13 "Restore Database" \
                        14 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $CHOICE in
            1) install_postgresql_if_needed ;;
            2) start_postgresql ;;
            3) stop_postgresql ;;
            4) restart_postgresql ;;
            5) enable_postgresql_at_boot ;;
            6) disable_postgresql_at_boot ;;
            7) secure_postgresql ;;
            8) create_database ;;
            9) create_table ;;
            10) insert_data ;;
            11) query_data ;;
            12) backup_database ;;
            13) restore_database ;;
            14) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Main script logic
detect_init_system
configure_postgresql

