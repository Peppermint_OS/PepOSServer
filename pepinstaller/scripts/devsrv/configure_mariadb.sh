#!/bin/bash

# Detect init system
detect_init_system() {
    if [[ -x "$(command -v systemctl)" ]]; then
        INIT_SYSTEM="systemd"
    elif [[ -x "$(command -v service)" ]]; then
        INIT_SYSTEM="sysvinit"
    elif [[ -x "$(command -v rc-service)" ]]; then
        INIT_SYSTEM="openrc"
    elif [[ -x "$(command -v runsvdir)" ]]; then
        INIT_SYSTEM="runit"
    else
        echo "Unsupported init system. Exiting."
        exit 1
    fi
}

# Function to install MariaDB if not installed
install_mariadb_if_needed() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            if ! dpkg -l mariadb-server > /dev/null 2>&1; then
                echo "MariaDB is not installed. Installing..."
                if sudo apt-get install -y mariadb-server; then
                    echo "MariaDB installed successfully."
                else
                    echo "Failed to install MariaDB. Exiting."
                    exit 1
                fi
            fi
            ;;
        openrc | runit)
            echo "MariaDB installation is managed differently in OpenRC or Runit."
            ;;
    esac
}

# Function to start MariaDB
start_mariadb() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service mariadb start
            ;;
        openrc)
            sudo rc-service mariadb start
            ;;
        runit)
            echo "Runit does not manage MariaDB."
            ;;
    esac
    dialog --msgbox "MariaDB started." 10 30
}

# Function to stop MariaDB
stop_mariadb() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service mariadb stop
            ;;
        openrc)
            sudo rc-service mariadb stop
            ;;
        runit)
            echo "Runit does not manage MariaDB."
            ;;
    esac
    dialog --msgbox "MariaDB stopped." 10 30
}

# Function to restart MariaDB
restart_mariadb() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service mariadb restart
            ;;
        openrc)
            sudo rc-service mariadb restart
            ;;
        runit)
            echo "Runit does not manage MariaDB."
            ;;
    esac
    dialog --msgbox "MariaDB restarted." 10 30
}

# Function to enable MariaDB at boot
enable_mariadb_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo systemctl enable mariadb
            ;;
        openrc)
            sudo rc-update add mariadb default
            ;;
        runit)
            echo "Runit does not manage MariaDB."
            ;;
    esac
    dialog --msgbox "MariaDB enabled at boot." 10 30
}

# Function to disable MariaDB at boot
disable_mariadb_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo systemctl disable mariadb
            ;;
        openrc)
            sudo rc-update del mariadb default
            ;;
        runit)
            echo "Runit does not manage MariaDB."
            ;;
    esac
    dialog --msgbox "MariaDB disabled at boot." 10 30
}

# Function to secure MariaDB installation
secure_mariadb() {
    sudo mysql_secure_installation
    dialog --msgbox "MariaDB installation secured." 10 30
}

# Function to create a database
create_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        mysql -e "CREATE DATABASE IF NOT EXISTS $DATABASE;"
        dialog --msgbox "Database '$DATABASE' created successfully." 10 60
    fi
}

# Function to create a table
create_table() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to create:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        mysql -e "USE $DATABASE; CREATE TABLE IF NOT EXISTS $TABLE (id INT AUTO_INCREMENT PRIMARY KEY);"
        dialog --msgbox "Table '$TABLE' created in database '$DATABASE' successfully." 10 60
    fi
}

# Function to insert data into a table
insert_data() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to insert data into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    DATA=$(dialog --inputbox "Enter data to insert into table (e.g., 'value1, value2'):" 10 60 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" && -n "$DATA" ]]; then
        mysql -e "USE $DATABASE; INSERT INTO $TABLE VALUES ($DATA);"
        dialog --msgbox "Data inserted into table '$TABLE' in database '$DATABASE' successfully." 10 60
    fi
}

# Function to query data from a table
query_data() {
    DATABASE=$(dialog --inputbox "Enter the name of the database:" 10 40 3>&1 1>&2 2>&3 3>&-)
    TABLE=$(dialog --inputbox "Enter the name of the table to query from:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" && -n "$TABLE" ]]; then
        QUERY=$(dialog --inputbox "Enter SQL query (e.g., 'SELECT * FROM $TABLE;'):" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -n "$QUERY" ]]; then
            mysql -e "USE $DATABASE; $QUERY"
            dialog --msgbox "Query executed successfully." 10 60
        fi
    fi
}

# Function to backup the database
backup_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to backup:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        mysqldump $DATABASE > $DATABASE.sql
        dialog --msgbox "Database '$DATABASE' backed up to '$DATABASE.sql' successfully." 10 60
    fi
}

# Function to restore the database
restore_database() {
    DATABASE=$(dialog --inputbox "Enter the name of the database to restore into:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DATABASE" ]]; then
        FILE=$(dialog --inputbox "Enter the path to the SQL file to restore:" 10 60 3>&1 1>&2 2>&3 3>&-)
        if [[ -f "$FILE" ]]; then
            mysql $DATABASE < $FILE
            dialog --msgbox "Database '$DATABASE' restored successfully." 10 60
        else
            dialog --msgbox "File not found or invalid." 10 60
        fi
    fi
}

# Function to configure MariaDB options
configure_mariadb() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure MariaDB" \
                        --title "MariaDB Menu" \
                        --menu "Choose an option:" 20 60 14 \
                        1 "Install/Check MariaDB" \
                        2 "Start MariaDB" \
                        3 "Stop MariaDB" \
                        4 "Restart MariaDB" \
                        5 "Enable MariaDB at Boot" \
                        6 "Disable MariaDB at Boot" \
                        7 "Secure MariaDB Installation" \
                        8 "Create Database" \
                        9 "Create Table" \
                        10 "Insert Data into Table" \
                        11 "Query Data from Table" \
                        12 "Backup Database" \
                        13 "Restore Database" \
                        14 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $CHOICE in
            1) install_mariadb_if_needed ;;
            2) start_mariadb ;;
            3) stop_mariadb ;;
            4) restart_mariadb ;;
            5) enable_mariadb_at_boot ;;
            6) disable_mariadb_at_boot ;;
            7) secure_mariadb ;;
            8) create_database ;;
            9) create_table ;;
            10) insert_data ;;
            11) query_data ;;
            12) backup_database ;;
            13) restore_database ;;
            14) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Main script logic
detect_init_system
configure_mariadb

