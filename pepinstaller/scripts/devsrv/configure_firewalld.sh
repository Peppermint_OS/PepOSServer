#!/bin/bash

# Detect init system
detect_init_system() {
    if [[ -x "$(command -v systemctl)" ]]; then
        INIT_SYSTEM="systemd"
    elif [[ -x "$(command -v service)" ]]; then
        INIT_SYSTEM="sysvinit"
    elif [[ -x "$(command -v rc-service)" ]]; then
        INIT_SYSTEM="openrc"
    elif [[ -x "$(command -v runsvdir)" ]]; then
        INIT_SYSTEM="runit"
    else
        echo "Unsupported init system. Exiting."
        exit 1
    fi
}

# Function to enable firewalld
enable_firewalld() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service firewalld start
            sudo chkconfig firewalld on
            ;;
        openrc)
            sudo rc-service firewalld start
            sudo rc-update add firewalld default
            ;;
        runit)
            echo "Runit does not use firewalld."
            ;;
    esac
    dialog --msgbox "firewalld enabled and started." 10 30
}

# Function to disable firewalld
disable_firewalld() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service firewalld stop
            sudo chkconfig firewalld off
            ;;
        openrc)
            sudo rc-service firewalld stop
            sudo rc-update del firewalld default
            ;;
        runit)
            echo "Runit does not use firewalld."
            ;;
    esac
    dialog --msgbox "firewalld disabled and stopped." 10 30
}

# Function to add service to firewalld
add_service() {
    SERVICE=$(dialog --inputbox "Enter the service name to add (e.g., ssh, http, https):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$SERVICE" ]; then
        dialog --msgbox "Please enter a valid service name." 10 30
    else
        case $INIT_SYSTEM in
            systemd | sysvinit)
                firewall-cmd --permanent --add-service=$SERVICE
                firewall-cmd --reload
                ;;
            openrc | runit)
                echo "Firewalld is not used with OpenRC or Runit."
                ;;
        esac
        dialog --msgbox "Service $SERVICE added to firewalld." 10 30
    fi
}

# Function to remove service from firewalld
remove_service() {
    SERVICE=$(dialog --inputbox "Enter the service name to remove:" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$SERVICE" ]; then
        dialog --msgbox "Please enter a valid service name." 10 30
    else
        case $INIT_SYSTEM in
            systemd | sysvinit)
                firewall-cmd --permanent --remove-service=$SERVICE
                firewall-cmd --reload
                ;;
            openrc | runit)
                echo "Firewalld is not used with OpenRC or Runit."
                ;;
        esac
        dialog --msgbox "Service $SERVICE removed from firewalld." 10 30
    fi
}

# Function to add port to firewalld
add_port() {
    PORT=$(dialog --inputbox "Enter the port to add (e.g., 8080/tcp):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$PORT" ]; then
        dialog --msgbox "Please enter a valid port." 10 30
    else
        case $INIT_SYSTEM in
            systemd | sysvinit)
                firewall-cmd --permanent --add-port=$PORT
                firewall-cmd --reload
                ;;
            openrc | runit)
                echo "Firewalld is not used with OpenRC or Runit."
                ;;
        esac
        dialog --msgbox "Port $PORT added to firewalld." 10 30
    fi
}

# Function to remove port from firewalld
remove_port() {
    PORT=$(dialog --inputbox "Enter the port to remove (e.g., 8080/tcp):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$PORT" ]; then
        dialog --msgbox "Please enter a valid port." 10 30
    else
        case $INIT_SYSTEM in
            systemd | sysvinit)
                firewall-cmd --permanent --remove-port=$PORT
                firewall-cmd --reload
                ;;
            openrc | runit)
                echo "Firewalld is not used with OpenRC or Runit."
                ;;
        esac
        dialog --msgbox "Port $PORT removed from firewalld." 10 30
    fi
}

# Function to configure zones in firewalld
configure_zones() {
    ZONE=$(dialog --inputbox "Enter the zone name (e.g., public, home, work):" 10 50 3>&1 1>&2 2>&3 3>&-)
    INTERFACE=$(dialog --inputbox "Enter the interface name (e.g., eth0, wlan0):" 10 50 3>&1 1>&2 2>&3 3>&-)
    
    # Check if user canceled
    if [ $? -eq 1 ]; then
        return 1
    fi
    
    if [ -z "$ZONE" ] || [ -z "$INTERFACE" ]; then
        dialog --msgbox "Please enter valid zone and interface names." 10 30
    else
        case $INIT_SYSTEM in
            systemd | sysvinit)
                firewall-cmd --zone=$ZONE --add-interface=$INTERFACE --permanent
                firewall-cmd --reload
                ;;
            openrc | runit)
                echo "Firewalld is not used with OpenRC or Runit."
                ;;
        esac
        dialog --msgbox "Interface $INTERFACE added to zone $ZONE." 10 30
    fi
}

# Function to configure firewalld options
configure_firewalld() {
    while true; do
        FIREWALL_CHOICE=$(dialog --clear --backtitle "Firewalld Configuration" \
                                --title "Firewalld Menu" \
                                --menu "Choose an option:" 20 60 10 \
                                1 "Enable firewalld" \
                                2 "Disable firewalld" \
                                3 "Add Service" \
                                4 "Remove Service" \
                                5 "Add Port" \
                                6 "Remove Port" \
                                7 "Configure Zones" \
                                8 "Back to Main Menu" \
                                3>&1 1>&2 2>&3 3>&-)

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $FIREWALL_CHOICE in
            1) enable_firewalld ;;
            2) disable_firewalld ;;
            3) add_service ;;
            4) remove_service ;;
            5) add_port ;;
            6) remove_port ;;
            7) configure_zones ;;
            8) break ;;
            *) dialog --msgbox "Invalid option. Please choose a valid option." 10 30 ;;
        esac
    done
}

# Main script logic
detect_init_system
configure_firewalld

