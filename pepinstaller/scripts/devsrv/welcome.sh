#!/bin/bash

# Directory where the scripts are located
SCRIPT_DIR="/usr/local/bin"

# Function to display the main menu
main_menu() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "PeppermintOS Server Configuration" \
                        --title "Main Menu" \
                        --menu "Choose an option:" 20 60 15 \
                        1 "Configure SSH" \
                        2 "Configure Static IP" \
                        3 "Update and Install Packages" \
                        4 "Configure firewalld" \
                        5 "Configure Hostname" \
                        6 "Create User" \
                        7 "Configure Nginx" \
                        8 "Configure Apache2" \
                        9 "Configure Postfix" \
                        10 "Configure MariaDB" \
                        11 "Configure PostgreSQL" \
                        12 "Configure SQLite" \
                        13 "Configure PHP and Docker" \
                        14 "Exit" \
                        3>&1 1>&2 2>&3)

        # Check if user canceled or exited
        if [[ $? -ne 0 ]]; then
            clear
            echo "Menu closed or canceled. Exiting..."
            exit 0
        fi

        clear

        case $CHOICE in
            1) sudo "$SCRIPT_DIR/configure_ssh.sh" ;;
            2) sudo "$SCRIPT_DIR/configure_static_ip.sh" ;;
            3) sudo "$SCRIPT_DIR/update_and_install.sh" ;;
            4) sudo "$SCRIPT_DIR/configure_firewalld.sh" ;;
            5) sudo "$SCRIPT_DIR/configure_hostname.sh" ;;
            6) sudo "$SCRIPT_DIR/create_user.sh" ;;
            7) sudo "$SCRIPT_DIR/configure_nginx.sh" ;;
            8) sudo "$SCRIPT_DIR/configure_apache2.sh" ;;
            9) sudo "$SCRIPT_DIR/configure_postfix.sh" ;;
            10) sudo "$SCRIPT_DIR/configure_mariadb.sh" ;;
            11) sudo "$SCRIPT_DIR/configure_postgresql.sh" ;;
            12) sudo "$SCRIPT_DIR/configure_sqlite.sh" ;;
            13) sudo "$SCRIPT_DIR/configure_php_and_docker.sh" ;;
            14) clear; echo "Exiting..."; exit 0 ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Show welcome message
dialog --msgbox "Welcome to PeppermintOS Server Friendly Configuration Tool!
This tool will help you configure various aspects of your server, including:

1. SSH: Configure the SSH server and client for secure remote access.
2. Static IP: Set a static IP address for consistent network communication.
3. Update and Install Packages: Ensure your system is up-to-date and install essential packages.
4. firewalld: Set up firewall rules to secure your server.
5. Hostname: Change the hostname of your server.
6. Create User: Add new users to your system.
7. Nginx: Configure the Nginx web server.
8. Apache2: Configure the Apache2 web server.
9. Postfix: Configure the Postfix mail server.
10. MariaDB: Set up the MariaDB database server.
11. PostgreSQL: Set up the PostgreSQL database server.
12. SQLite: Configure the SQLite database.
13. PHP: Configure PHP and related settings.
14. Docker: Configure Docker and manage containers.
15. Exit: Exit the configuration tool.

Please select an option from the menu to begin." 20 60

# Display main menu
main_menu

