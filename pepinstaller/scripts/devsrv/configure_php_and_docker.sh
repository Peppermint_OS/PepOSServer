#!/bin/bash

# Function to install PHP if not installed
install_php_if_needed() {
    if ! dpkg -l php > /dev/null 2>&1; then
        echo "PHP is not installed. Installing..."
        if sudo apt-get install -y php; then
            echo "PHP installed successfully."
        else
            echo "Failed to install PHP. Exiting."
            exit 1
        fi
    fi
}

# Function to configure PHP
configure_php() {
    while true; do
        OPTION=$(dialog --clear --backtitle "Configure PHP" \
                        --title "PHP Configuration Menu" \
                        --menu "Choose an option:" 15 60 4 \
                        1 "Configure PHP.ini" \
                        2 "Set PHP Error Reporting" \
                        3 "Set PHP Timezone" \
                        4 "Return to Main Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $OPTION in
            1) sudo nano /etc/php/7.4/apache2/php.ini ;; # Adjust version if needed
            2) echo "error_reporting = E_ALL" | sudo tee -a /etc/php/7.4/apache2/php.ini ;; # Adjust version if needed
            3) TZ=$(dialog --inputbox "Enter PHP timezone (e.g., America/New_York):" 10 40 3>&1 1>&2 2>&3)
               sudo sed -i "s|^;date.timezone =|date.timezone = $TZ|" /etc/php/7.4/apache2/php.ini ;; # Adjust version if needed
            4) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Function to install Docker if not installed
install_docker_if_needed() {
    if ! command -v docker &> /dev/null; then
        echo "Docker is not installed. Installing..."
        if curl -fsSL https://get.docker.com | sudo sh; then
            echo "Docker installed successfully."
        else
            echo "Failed to install Docker. Exiting."
            exit 1
        fi
    fi
}

# Function to configure Docker
configure_docker() {
    while true; do
        OPTION=$(dialog --clear --backtitle "Configure Docker" \
                        --title "Docker Configuration Menu" \
                        --menu "Choose an option:" 15 60 6 \
                        1 "Create Docker Network" \
                        2 "Manage Docker Containers" \
                        3 "Manage Docker Images" \
                        4 "Manage Docker Volumes" \
                        5 "Manage Docker Compose" \
                        6 "Return to Main Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $OPTION in
            1) NETWORK=$(dialog --inputbox "Enter Docker network name:" 10 40 3>&1 1>&2 2>&3)
               sudo docker network create $NETWORK ;;
            2) manage_docker_containers ;;
            3) manage_docker_images ;;
            4) manage_docker_volumes ;;
            5) manage_docker_compose ;;
            6) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Function to manage Docker containers
manage_docker_containers() {
    while true; do
        ACTION=$(dialog --clear --backtitle "Manage Docker Containers" \
                        --title "Docker Container Management" \
                        --menu "Choose an action:" 15 60 4 \
                        1 "List Containers" \
                        2 "Start Container" \
                        3 "Stop Container" \
                        4 "Return to Docker Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $ACTION in
            1) sudo docker ps -a ;;
            2) CONTAINER=$(dialog --inputbox "Enter the container ID or name:" 10 40 3>&1 1>&2 2>&3)
               sudo docker start $CONTAINER ;;
            3) CONTAINER=$(dialog --inputbox "Enter the container ID or name:" 10 40 3>&1 1>&2 2>&3)
               sudo docker stop $CONTAINER ;;
            4) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Function to manage Docker images
manage_docker_images() {
    while true; do
        ACTION=$(dialog --clear --backtitle "Manage Docker Images" \
                        --title "Docker Image Management" \
                        --menu "Choose an action:" 15 60 4 \
                        1 "List Images" \
                        2 "Pull Image" \
                        3 "Remove Image" \
                        4 "Return to Docker Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $ACTION in
            1) sudo docker images ;;
            2) IMAGE=$(dialog --inputbox "Enter the image name (e.g., ubuntu):" 10 40 3>&1 1>&2 2>&3)
               sudo docker pull $IMAGE ;;
            3) IMAGE=$(dialog --inputbox "Enter the image ID or name:" 10 40 3>&1 1>&2 2>&3)
               sudo docker rmi $IMAGE ;;
            4) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Function to manage Docker volumes
manage_docker_volumes() {
    while true; do
        ACTION=$(dialog --clear --backtitle "Manage Docker Volumes" \
                        --title "Docker Volume Management" \
                        --menu "Choose an action:" 15 60 4 \
                        1 "List Volumes" \
                        2 "Create Volume" \
                        3 "Remove Volume" \
                        4 "Return to Docker Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $ACTION in
            1) sudo docker volume ls ;;
            2) VOLUME=$(dialog --inputbox "Enter the volume name:" 10 40 3>&1 1>&2 2>&3)
               sudo docker volume create $VOLUME ;;
            3) VOLUME=$(dialog --inputbox "Enter the volume name or ID:" 10 40 3>&1 1>&2 2>&3)
               sudo docker volume rm $VOLUME ;;
            4) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Function to manage Docker Compose
manage_docker_compose() {
    while true; do
        ACTION=$(dialog --clear --backtitle "Manage Docker Compose" \
                        --title "Docker Compose Management" \
                        --menu "Choose an action:" 15 60 4 \
                        1 "Run Docker Compose" \
                        2 "Stop Docker Compose" \
                        3 "Remove Docker Compose" \
                        4 "Return to Docker Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $ACTION in
            1) docker_compose_up ;;
            2) docker_compose_down ;;
            3) docker_compose_remove ;;
            4) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Function to run Docker Compose
docker_compose_up() {
    COMPOSE_FILE=$(dialog --inputbox "Enter the Docker Compose file (e.g., docker-compose.yaml):" 10 40 3>&1 1>&2 2>&3)
    if [[ -n "$COMPOSE_FILE" ]]; then
        sudo docker-compose -f $COMPOSE_FILE up -d
    fi
}

# Function to stop Docker Compose
docker_compose_down() {
    COMPOSE_FILE=$(dialog --inputbox "Enter the Docker Compose file (e.g., docker-compose.yaml):" 10 40 3>&1 1>&2 2>&3)
    if [[ -n "$COMPOSE_FILE" ]]; then
        sudo docker-compose -f $COMPOSE_FILE down
    fi
}

# Function to remove Docker Compose
docker_compose_remove() {
    COMPOSE_FILE=$(dialog --inputbox "Enter the Docker Compose file (e.g., docker-compose.yaml):" 10 40 3>&1 1>&2 2>&3)
    if [[ -n "$COMPOSE_FILE" ]]; then
        sudo docker-compose -f $COMPOSE_FILE down --volumes --remove-orphans
    fi
}

# Function to display the main menu
main_menu() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Server Utilities Installation and Configuration" \
                        --title "Main Menu" \
                        --menu "Choose an option:" 15 60 5 \
                        1 "Install/Check PHP" \
                        2 "Configure PHP" \
                        3 "Install/Check Docker" \
                        4 "Configure Docker" \
                        5 "Return to Main Menu" \
                        3>&1 1>&2 2>&3)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $CHOICE in
            1) install_php_if_needed ;;
            2) configure_php ;;
            3) install_docker_if_needed ;;
            4) configure_docker ;;
            5) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done

    echo "Server utilities installation and configuration script completed."
}

# Display main menu
main_menu

