#!/bin/bash

# Detect init system
detect_init_system() {
    if [[ -x "$(command -v systemctl)" ]]; then
        INIT_SYSTEM="systemd"
    elif [[ -x "$(command -v service)" ]]; then
        INIT_SYSTEM="sysvinit"
    elif [[ -x "$(command -v rc-service)" ]]; then
        INIT_SYSTEM="openrc"
    elif [[ -x "$(command -v runsvdir)" ]]; then
        INIT_SYSTEM="runit"
    else
        echo "Unsupported init system. Exiting."
        exit 1
    fi
}

# Function to install Apache if not installed
install_apache_if_needed() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            if ! dpkg -l apache2 > /dev/null 2>&1; then
                echo "Apache is not installed. Installing..."
                if sudo apt-get install -y apache2; then
                    echo "Apache installed successfully."
                else
                    echo "Failed to install Apache. Exiting."
                    exit 1
                fi
            fi
            ;;
        openrc)
            if ! apk info -q apache2 > /dev/null 2>&1; then
                echo "Apache is not installed. Installing..."
                if sudo apk add apache2; then
                    echo "Apache installed successfully."
                else
                    echo "Failed to install Apache. Exiting."
                    exit 1
                fi
            fi
            ;;
        runit)
            echo "Runit does not require Apache installation."
            ;;
    esac
}

# Function to start Apache
start_apache() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service apache2 start
            ;;
        openrc)
            sudo rc-service apache2 start
            ;;
        runit)
            echo "Use 'sv start apache2' to start Apache with runit."
            ;;
    esac
    dialog --msgbox "Apache started." 10 30
}

# Function to stop Apache
stop_apache() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service apache2 stop
            ;;
        openrc)
            sudo rc-service apache2 stop
            ;;
        runit)
            echo "Use 'sv stop apache2' to stop Apache with runit."
            ;;
    esac
    dialog --msgbox "Apache stopped." 10 30
}

# Function to restart Apache
restart_apache() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo service apache2 restart
            ;;
        openrc)
            sudo rc-service apache2 restart
            ;;
        runit)
            echo "Use 'sv restart apache2' to restart Apache with runit."
            ;;
    esac
    dialog --msgbox "Apache restarted." 10 30
}

# Function to enable Apache at boot
enable_apache_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo update-rc.d apache2 enable
            ;;
        openrc)
            sudo rc-update add apache2 default
            ;;
        runit)
            echo "Runit does not require enabling services at boot."
            ;;
    esac
    dialog --msgbox "Apache enabled at boot." 10 30
}

# Function to disable Apache at boot
disable_apache_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            sudo update-rc.d apache2 disable
            ;;
        openrc)
            sudo rc-update del apache2 default
            ;;
        runit)
            echo "Runit does not require disabling services at boot."
            ;;
    esac
    dialog --msgbox "Apache disabled at boot." 10 30
}

# Function to configure Apache virtual hosts
configure_apache_virtual_hosts() {
    DOMAIN=$(dialog --inputbox "Enter the domain name for the virtual host (e.g., example.com):" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -n "$DOMAIN" ]]; then
        case $INIT_SYSTEM in
            systemd | sysvinit)
                sudo mkdir -p /var/www/$DOMAIN/public_html
                sudo chown -R www-data:www-data /var/www/$DOMAIN/public_html
                sudo chmod -R 755 /var/www/$DOMAIN
                cat << EOF | sudo tee /etc/apache2/sites-available/$DOMAIN.conf > /dev/null
<VirtualHost *:80>
    ServerAdmin webmaster@$DOMAIN
    ServerName $DOMAIN
    DocumentRoot /var/www/$DOMAIN/public_html
    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF
                sudo a2ensite $DOMAIN.conf
                sudo service apache2 reload
                ;;
            openrc)
                sudo mkdir -p /var/www/$DOMAIN/public_html
                sudo chown -R apache:apache /var/www/$DOMAIN/public_html
                sudo chmod -R 755 /var/www/$DOMAIN
                cat << EOF | sudo tee /etc/apache2/sites-available/$DOMAIN.conf > /dev/null
<VirtualHost *:80>
    ServerAdmin webmaster@$DOMAIN
    ServerName $DOMAIN
    DocumentRoot /var/www/$DOMAIN/public_html
    ErrorLog /var/log/apache2/$DOMAIN_error.log
    CustomLog /var/log/apache2/$DOMAIN_access.log combined
</VirtualHost>
EOF
                sudo a2ensite $DOMAIN.conf
                sudo service apache2 reload
                ;;
            runit)
                echo "Runit does not use Apache virtual hosts in the same way."
                ;;
        esac
        dialog --msgbox "Virtual host for $DOMAIN configured successfully." 10 60
    fi
}

# Function to configure Apache
configure_apache() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Configure Apache" \
                        --title "Apache Menu" \
                        --menu "Choose an option:" 20 60 11 \
                        1 "Install/Check Apache" \
                        2 "Start Apache" \
                        3 "Stop Apache" \
                        4 "Restart Apache" \
                        5 "Enable Apache at Boot" \
                        6 "Disable Apache at Boot" \
                        7 "Secure Apache Installation" \
                        8 "Configure Virtual Host" \
                        9 "Enable/Disable Apache Site" \
                        10 "Enable/Disable Apache Module" \
                        11 "Configure Certbot" \
                        12 "Return to Main Menu" \
                        3>&1 1>&2 2>&3 3>&-)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $CHOICE in
            1) install_apache_if_needed ;;
            2) start_apache ;;
            3) stop_apache ;;
            4) restart_apache ;;
            5) enable_apache_at_boot ;;
            6) disable_apache_at_boot ;;
            7) secure_apache ;;
            8) configure_apache_virtual_hosts ;;
            9) enable_disable_apache_site ;;
            10) enable_disable_apache_module ;;
            11) configure_certbot ;;
            12) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done
}

# Main script logic
detect_init_system
configure_apache


