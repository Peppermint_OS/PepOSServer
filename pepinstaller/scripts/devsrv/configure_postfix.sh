#!/bin/bash

# Detect init system
detect_init_system() {
    if [[ -x "$(command -v systemctl)" ]]; then
        INIT_SYSTEM="systemd"
    elif [[ -x "$(command -v service)" ]]; then
        INIT_SYSTEM="sysvinit"
    elif [[ -x "$(command -v rc-service)" ]]; then
        INIT_SYSTEM="openrc"
    elif [[ -x "$(command -v runsvdir)" ]]; then
        INIT_SYSTEM="runit"
    else
        echo "Unsupported init system. Exiting."
        exit 1
    fi
}

# Function to install Postfix if not installed
install_postfix_if_needed() {
    case $INIT_SYSTEM in
        systemd | sysvinit | openrc | runit)
            if ! dpkg -l postfix > /dev/null 2>&1; then
                echo "Postfix is not installed. Installing..."
                if sudo apt-get install -y postfix; then
                    echo "Postfix installed successfully."
                else
                    echo "Failed to install Postfix. Exiting."
                    exit 1
                fi
            fi
            ;;
        *)
            echo "Unsupported init system. Cannot install Postfix."
            ;;
    esac
}

# Function to configure Postfix interactively
configure_postfix() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Configuring Postfix..."
            if ! sudo dpkg-reconfigure postfix; then
                echo "Failed to configure Postfix."
                dialog --title "Error" --msgbox "Failed to configure Postfix." 10 60
                exit 1
            fi
            echo "Postfix configured successfully."
            dialog --title "Success" --msgbox "Postfix configured successfully." 10 60
            ;;
        openrc | runit)
            echo "Postfix configuration is managed differently in OpenRC or Runit."
            ;;
    esac
}

# Function to start Postfix service
start_postfix() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Starting Postfix service..."
            if ! sudo systemctl start postfix; then
                echo "Failed to start Postfix service."
                dialog --title "Error" --msgbox "Failed to start Postfix service." 10 60
                exit 1
            fi
            echo "Postfix service started successfully."
            dialog --title "Success" --msgbox "Postfix service started successfully." 10 60
            ;;
        openrc)
            sudo rc-service postfix start
            dialog --title "Success" --msgbox "Postfix service started successfully." 10 60
            ;;
        runit)
            echo "Runit does not manage Postfix."
            ;;
    esac
}

# Function to stop Postfix service
stop_postfix() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Stopping Postfix service..."
            if ! sudo systemctl stop postfix; then
                echo "Failed to stop Postfix service."
                dialog --title "Error" --msgbox "Failed to stop Postfix service." 10 60
                exit 1
            fi
            echo "Postfix service stopped successfully."
            dialog --title "Success" --msgbox "Postfix service stopped successfully." 10 60
            ;;
        openrc)
            sudo rc-service postfix stop
            dialog --title "Success" --msgbox "Postfix service stopped successfully." 10 60
            ;;
        runit)
            echo "Runit does not manage Postfix."
            ;;
    esac
}

# Function to restart Postfix service
restart_postfix() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Restarting Postfix service..."
            if ! sudo systemctl restart postfix; then
                echo "Failed to restart Postfix service."
                dialog --title "Error" --msgbox "Failed to restart Postfix service." 10 60
                exit 1
            fi
            echo "Postfix service restarted successfully."
            dialog --title "Success" --msgbox "Postfix service restarted successfully." 10 60
            ;;
        openrc)
            sudo rc-service postfix restart
            dialog --title "Success" --msgbox "Postfix service restarted successfully." 10 60
            ;;
        runit)
            echo "Runit does not manage Postfix."
            ;;
    esac
}

# Function to enable Postfix service at boot
enable_postfix_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Enabling Postfix service at boot..."
            if ! sudo systemctl enable postfix; then
                echo "Failed to enable Postfix service at boot."
                dialog --title "Error" --msgbox "Failed to enable Postfix service at boot." 10 60
                exit 1
            fi
            echo "Postfix service enabled at boot successfully."
            dialog --title "Success" --msgbox "Postfix service enabled at boot successfully." 10 60
            ;;
        openrc)
            sudo rc-update add postfix default
            dialog --title "Success" --msgbox "Postfix service enabled at boot successfully." 10 60
            ;;
        runit)
            echo "Runit does not manage Postfix."
            ;;
    esac
}

# Function to disable Postfix service at boot
disable_postfix_at_boot() {
    case $INIT_SYSTEM in
        systemd | sysvinit)
            echo "Disabling Postfix service at boot..."
            if ! sudo systemctl disable postfix; then
                echo "Failed to disable Postfix service at boot."
                dialog --title "Error" --msgbox "Failed to disable Postfix service at boot." 10 60
                exit 1
            fi
            echo "Postfix service disabled at boot successfully."
            dialog --title "Success" --msgbox "Postfix service disabled at boot successfully." 10 60
            ;;
        openrc)
            sudo rc-update del postfix default
            dialog --title "Success" --msgbox "Postfix service disabled at boot successfully." 10 60
            ;;
        runit)
            echo "Runit does not manage Postfix."
            ;;
    esac
}

# Function to configure Postfix securely (optional)
secure_postfix() {
    echo "Securing Postfix configuration..."
    # You can add additional secure configuration steps here if needed
    echo "Postfix configuration secured successfully."
    dialog --title "Success" --msgbox "Postfix configuration secured successfully." 10 60
}

# Function to display the main menu
main_menu() {
    while true; do
        CHOICE=$(dialog --clear --backtitle "Postfix Configuration" \
                        --title "Postfix Menu" \
                        --menu "Choose an option:" 15 60 9 \
                        1 "Install/Check Postfix" \
                        2 "Configure Postfix" \
                        3 "Start Postfix" \
                        4 "Stop Postfix" \
                        5 "Restart Postfix" \
                        6 "Enable Postfix at Boot" \
                        7 "Disable Postfix at Boot" \
                        8 "Secure Postfix Configuration" \
                        9 "Exit" \
                        3>&1 1>&2 2>&3 3>&-)

        clear

        # Check if user canceled
        if [ $? -eq 1 ]; then
            break
        fi

        case $CHOICE in
            1) install_postfix_if_needed ;;
            2) configure_postfix ;;
            3) start_postfix ;;
            4) stop_postfix ;;
            5) restart_postfix ;;
            6) enable_postfix_at_boot ;;
            7) disable_postfix_at_boot ;;
            8) secure_postfix ;;
            9) break ;;
            *) dialog --msgbox "Invalid option." 10 30 ;;
        esac
    done

    echo "Postfix configuration script completed."
}

# Main script logic
detect_init_system
main_menu

