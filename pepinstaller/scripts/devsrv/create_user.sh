#!/bin/bash

# Function to create users
create_user() {
    local USERNAME
    local FULLNAME
    local PASSWORD
    local GROUPS
    local SELECTED_GROUPS

    # Prompt for username
    USERNAME=$(dialog --inputbox "Enter the username of the new user:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -z "$USERNAME" ]]; then
        dialog --msgbox "Username cannot be empty. User creation canceled." 10 30
        return
    fi

    # Check if username already exists
    if id "$USERNAME" &>/dev/null; then
        dialog --msgbox "User $USERNAME already exists. User creation canceled." 10 30
        return
    fi

    # Prompt for full name
    FULLNAME=$(dialog --inputbox "Enter the full name of the new user:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -z "$FULLNAME" ]]; then
        dialog --msgbox "Full name cannot be empty. User creation canceled." 10 30
        return
    fi

    # Prompt for password
    PASSWORD=$(dialog --passwordbox "Enter the password for user $USERNAME:" 10 40 3>&1 1>&2 2>&3 3>&-)
    if [[ -z "$PASSWORD" ]]; then
        dialog --msgbox "Password cannot be empty. User creation canceled." 10 30
        return
    fi

    # Get list of available groups
    GROUPS=$(getent group | cut -d: -f1)
    GROUPS_ARR=()
    for group in $GROUPS; do
        GROUPS_ARR+=("$group" "" off)
    done

    # Select groups to add user
    SELECTED_GROUPS=$(dialog --checklist "Select groups to add user $USERNAME:" 20 60 10 "${GROUPS_ARR[@]}" 3>&1 1>&2 2>&3 3>&-)
    if [[ -z "$SELECTED_GROUPS" ]]; then
        dialog --msgbox "No groups selected. User $USERNAME will not be added to any groups." 10 30
    fi

    # Create the user
    useradd -m -c "$FULLNAME" "$USERNAME"
    if [[ $? -eq 0 ]]; then
        echo "$USERNAME:$PASSWORD" | chpasswd
        if [[ $? -eq 0 ]]; then
            dialog --msgbox "User $USERNAME created successfully." 10 30
            # Add user to selected groups
            for group in $SELECTED_GROUPS; do
                usermod -aG "$group" "$USERNAME"
                if [[ $? -ne 0 ]]; then
                    dialog --msgbox "Failed to add user $USERNAME to group $group." 10 30
                fi
            done
        else
            dialog --msgbox "Failed to set password for user $USERNAME. User creation canceled." 10 30
            userdel -r "$USERNAME" # Rollback user creation if password setting failed
        fi
    else
        dialog --msgbox "Failed to create user $USERNAME. User creation canceled." 10 30
    fi
}

# Call the function to create users
create_user

