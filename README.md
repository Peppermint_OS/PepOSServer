PeppermintOS Server ISO (In Development)

This project is a custom server ISO of PeppermintOS, based on Debian Installer and licensed under the GNU General Public License v3.0. The ISO is designed to be easy to install and to provide a lightweight, efficient server environment for a variety of uses.

The PeppermintOS Server ISO is still in development and is not yet available for download. However, you can build it locally if you wish to customize it. Once the ISO is available, you can follow the instructions below to install PeppermintOS Server:

    Burn the ISO image to a CD or create a bootable USB drive.
    Boot your computer from the CD or USB drive.
    Follow the on-screen instructions to install PeppermintOS Server using the Debian Installer.

Ensure that your computer meets the minimum system requirements for PeppermintOS Server. Also be aware that the installation process will erase all content on your computer's hard drive.
Features

This custom server ISO will include the following features:

    Installation of PeppermintOS Server using the Debian Installer.
    Lightweight server environment with minimal pre-installed software.
    Compatibility with amd64 architecture.

Contribution

This project is open source and contributions are welcome. If you would like to contribute, please follow the steps below:

    Fork this repository.
    Create a branch for your changes (git checkout -b my-branch).
    Make your changes and ensure they work.
    Submit a pull request to this repository.

All contributions are subject to the GPL 3 license.
License

This project is licensed under the GNU General Public License v3.0. See the LICENSE.md file for more information.
Author

This project was created by the PeppermintOS team. Please contact them if you have any questions or comments about the project.
Acknowledgments

We thank the Debian team for providing the Debian Installer and the lightweight server environment.
